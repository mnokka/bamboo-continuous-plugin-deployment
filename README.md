# Continuous Deployment for Atlassian Plugins #

A Bamboo plugin that provides an installation task for Atlassian Plugins, allowing you to set up a Continuous Integration ("CI") or Continuous Deployment ("CD") build for a plugin project.

Changed to work in my demo environment:
Demo Atlassian plugin change Bamboo autodeployed to Confluence server (local development version using ngrok for Bamboo connection)
Bypassed failing after deployment test.

## Installation ##

If you want to use this plugin, you can download the latest version from the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.atlassian.bamboo.plugins.deploy.continuous-plugin-deployment).

## Usage ##

This plugin adds a single task, the 'Plugin Deploy Task', to your Bamboo toolbelt. You can add this task to any of your Bamboo builds to add a deployment step to an Atlassian application.

The task consumes a Bamboo artifact that must be produced by a job in an earlier stage of the Build (or made available through an Artifact Download Task). If you are not familiar with Bamboo's artifact sharing feature you should read the [corresponding documentation](https://confluence.atlassian.com/display/BAMBOO/Sharing+artifacts), as without this knowledge you will find it difficult to configure this task correctly.

**Here's a quick video showing the configuration of the task in action: [http://youtu.be/acRyXUVVHtg](http://youtu.be/acRyXUVVHtg)**

## Building ##

To build this plugin from source, use the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project). This project uses the [Karma](http://karma-runner.github.io/) project to run JavaScript unit tests, so you will need Node.js installed, and the Karma runner package. Once you have Node.js installed, installing the Karma packages is as easy as running the following command from the root directory.

	npm install

From here, you can compile the project and execute all tests using:

	atlas-mvn clean install

If you want to run the Karma tests separately, you can execute:

	node_modules/karma/bin/karma start

The other tests in the project are a mixture of JUnit and JWebUnit tests that the Plugin SDK should handle automatically for you.

## License ##

    Copyright (c) 2011-2014, Atlassian Pty. Ltd.
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    The names of contributors may not
    be used to endorse or promote products derived from this software without
    specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.