package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductLoginRequestBehaviour;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Login processor for Stash servers.
 */
public class StashLoginRequestBehaviour extends ProductLoginRequestBehaviour
{
    public StashLoginRequestBehaviour(final String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "/j_stash_security_check"), "j_username", "j_password");
    }

    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 302)
            {
                return Result.failure("Product login failed. HTTP Status code " + statusCode + " was returned.");

            }

            // Assume that if we get redirected to the login screen, the authentication failed. Sadly, it seems that
            // Stash provides no information in the response to categorically indicate if the login was successful or
            // not.
            Header location = response.getFirstHeader("Location");
            if (location.getValue().endsWith("/login"))
            {
                return Result.failure("Product login failed. We got redirected back to the login screen again");
            }

            return Result.success("Product login success!");
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
