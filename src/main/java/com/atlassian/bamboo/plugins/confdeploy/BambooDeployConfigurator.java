package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.task.TaskDefinition;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * {@inheritDoc}
 */
@Deprecated
public class BambooDeployConfigurator extends AutoDeployConfigurator
{
    @Override
    protected RemoteProductType getProduct()
    {
        return RemoteProductType.BAMBOO;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        context.put("deprecated", true);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        context.put("deprecated", true);

    }
}
