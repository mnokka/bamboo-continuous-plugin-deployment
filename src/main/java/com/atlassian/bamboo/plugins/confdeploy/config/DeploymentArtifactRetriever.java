package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.fugue.Either;

import java.io.File;

/**
 * Implements the {@link ArtifactRetriever} service for tasks within Deployment projects.
 */
public class DeploymentArtifactRetriever extends BaseArtifactRetriever implements ArtifactRetriever
{
    public DeploymentArtifactRetriever(final DeploymentTaskContext taskContext,
                                    final CustomVariableContext customVariableContext)
    {
        super(taskContext, customVariableContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<File, Failure> getFileFromArtifact(String artifactString)
    {
        AvailableArtifact artifact =  new AvailableArtifact(artifactString);
        if (!artifact.isFromTransferTask())
        {
            return Either.right(Result.failure("Configured plugin artifact was not acquired by a transfer task, but this is a deployment project. WTF?"));
        }

        // Ensure that the artifact exists and is accessible.
        File pluginJar = getArtifactFromTransferTask(artifact);
        if (pluginJar == null)
        {
            final String msg = String.format("Unable to find the artifact with id %d in this task's context. Has an artifact subscription been configured for that artifact in this job?", artifact.getArtifactId());
            return Either.right(Result.failure(msg));
        }

        if (!pluginJar.exists() || !pluginJar.isFile())
        {
            final String msg = String.format("Artifact file %s is either missing or not a file.", pluginJar.getAbsolutePath());
            return Either.right(Result.failure(msg));
        }
        return Either.left(pluginJar);

    }
}
