package com.atlassian.bamboo.plugins.confdeploy.upload.http;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import com.atlassian.util.concurrent.Timeout;
import com.google.common.base.Function;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * {@inheritDoc}
 */
public class DefaultHttpClientWrapper implements HttpClientWrapper
{
    private final BuildLogger buildLogger;
    private final HttpClient client;

    /**
     * It's a constructor.
     */
    public DefaultHttpClientWrapper(final BuildLogger buildLogger, final TaskConfiguration taskConfiguration)
    {
        this.buildLogger = buildLogger;
        this.client = initClient(taskConfiguration);
    }

    // Hides the gross X509TrustManager boilerplate code within a collapsible section in my IDE so I don't have to look
    // at it all the time.
    private HttpClient initClient(final TaskConfiguration taskConfiguration)
    {
        DefaultHttpClient client = new DefaultHttpClient();

        if(taskConfiguration.allowSSLCertificateErrors())
        {
            buildLogger.addBuildLogEntry("SSL Certificate check: disabled");
            try
            {
                X509TrustManager tm = new X509TrustManager() {
                    public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException
                    {
                        for (X509Certificate cert : certs)
                            buildLogger.addBuildLogEntry("Automatically accepting client certificate: " + cert.getSubjectDN());
                    }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException
                    {
                        for (X509Certificate cert : certs)
                            buildLogger.addBuildLogEntry("Automatically accepting server certificate: " + cert.getSubjectDN());
                    }
                    public X509Certificate[] getAcceptedIssuers()
                    {
                        return null;
                    }
                };
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(null, new TrustManager[]{tm}, null);
                SSLSocketFactory ssf = new SSLSocketFactory(ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                ClientConnectionManager ccm = client.getConnectionManager();
                SchemeRegistry sr = ccm.getSchemeRegistry();
                sr.register(new Scheme("https",443,ssf));
            }
            catch (NoSuchAlgorithmException e)
            {
                buildLogger.addErrorLogEntry("Could not find SSL Context for algorithm: TLS",e);
            }
            catch (KeyManagementException e)
            {
                buildLogger.addErrorLogEntry("Could not register custom TrustManager: "+e.getMessage(),e);
            }
        }
        else
        {
            buildLogger.addBuildLogEntry("SSL Certificate check: enabled");
        }

        if (taskConfiguration.getEnableTrafficLogging())
        {
            client.addRequestInterceptor(new BuildLoggingRequestInterceptor(buildLogger, taskConfiguration)); // Logger should be the last invoked request interceptor
            client.addResponseInterceptor(new BuildLoggingResponseInterceptor(buildLogger, taskConfiguration), 0); // Logger should be the first invoked response interceptor
        }

        // Atlassian ID uses httpOnly cookies that are not part of the default cookie spec used by HttpClient
        client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);

        // Persist the session cookie across requests so that, if WebSudo is enabled on the remote host, our requests are marked as secure once we get through
        // the initial WebSudo dance.
        CookieStore cookieStore = new BasicCookieStore();
        client.setCookieStore(cookieStore);

        return client;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result execute(final HttpUriRequest request, final ResponseHandler<Result> responseHandler)
    {
        try
        {
            return client.execute(request, responseHandler);
        }
        catch (IOException e)
        {
            return Result.failure("HTTP Request failed - an I/O Exception occurred: " + e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result poll(final HttpUriRequest initialRequest, final Function<HttpResponse, Either<HttpUriRequest, Result>> evaluator, final Timeout timeout)
    {
        return this.execute(initialRequest, new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws IOException
            {
                Either<HttpUriRequest, Result> nextAction = evaluator.apply(response);
                if (nextAction.isLeft())
                {
                    if (timeout.isExpired())
                    {
                        return Result.failure("Polling request did not complete within the timeout.");
                    }
                    return poll(nextAction.left().get(), evaluator, timeout);
                }
                else
                {
                    return nextAction.right().get();
                }
            }
        });
    }
}
