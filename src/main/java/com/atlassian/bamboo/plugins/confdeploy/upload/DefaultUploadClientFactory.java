package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.PollingRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso.AtlassianIdLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso.AtlassianIdWebSudoRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ondemand.OnDemandLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.*;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm.*;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.DefaultHttpClientWrapper;

/**
 * {@inheritDoc}
 */
public class DefaultUploadClientFactory implements UploadClientFactory
{

    /**
     * {@inheritDoc}
     */
    @Override
    public DefaultUploadClient getUploadClient(RemoteProductType productType, TaskConfiguration taskConfiguration, BuildLogger buildLogger)
    {
        RequestBehaviour upmSelfUpdateStrategy = null;
        PollingRequestBehaviour upmSelfUpdateCheckStrategy = null;
        if (isUpm(taskConfiguration))
        {
            upmSelfUpdateStrategy = new UpmSelfUpdateBehaviour(taskConfiguration.getRemoteBaseUrl());
            upmSelfUpdateCheckStrategy = new UpmSelfUpdateCheckBehaviour(taskConfiguration.getRemoteBaseUrl());
        }

        return new DefaultUploadClient(
                getLoginBehaviour(productType, taskConfiguration),
                getPermissionCheckStrategy(productType),
                getWebSudoBehaviour(productType, taskConfiguration),
                new UpmTokenRequestBehaviour(),
                new PluginUploadRequestBehaviour(),
                new UploadSuccessCheckBehaviour(taskConfiguration),
                upmSelfUpdateStrategy,
                upmSelfUpdateCheckStrategy,
                buildLogger,
                new DefaultHttpClientWrapper(buildLogger, taskConfiguration)
        );
    }

    /**
     * Returns {@code true} if the plugin being deployed is the Atlassian Universal Plugin Manager.
     */
    private boolean isUpm(final TaskConfiguration taskConfiguration)
    {
        return taskConfiguration.getPluginJar().getName().startsWith("atlassian-universal-plugin-manager-plugin");
    }

    /**
     * Returns {@code true} if the specified Base URL is an Atlassian OnDemand instance. I'm not yet 100% sure that this
     * logic can be relied upon.
     */
    private boolean isOnDemand(final TaskConfiguration taskConfiguration)
    {
        final String baseUrl = taskConfiguration.getRemoteBaseUrl();
        return baseUrl.contains(".atlassian.net") || baseUrl.contains(".jira.com") || baseUrl.contains(".jira-dev.com");
    }

    private RequestBehaviour getPermissionCheckStrategy(RemoteProductType productType)
    {
        switch (productType)
        {
            case JIRA:
                return new JiraPermissionCheckBehaviour();

            // Sadly, Confluence & Bamboo do not expose global permission information through their Remote APIs.
            // TODO: Is it possible with FeCru & Stash?
            default:
                return null;
        }
    }

    /**
     * Determines what kind of WebSudo dance the remote Atlassian application is expecting, and returns an appropriate
     * client implementation for that dance.
     */
    private RequestBehaviour getWebSudoBehaviour(RemoteProductType productType, TaskConfiguration taskConfiguration)
    {
        if (!productType.getSupportsWebSudo())
        {
            // Product does not support WebSudo.
            return null;
        }
        else if (taskConfiguration.useAtlassianId() && taskConfiguration.useAtlassianIdWebSudo())
        {
            return new AtlassianIdWebSudoRequestBehaviour();
        }
        else if (productType == RemoteProductType.CONFLUENCE)
        {
            return new ConfluenceWebSudoRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
        }
        else if (productType == RemoteProductType.JIRA)
        {
            return new JiraWebSudoRequestBehaviour(taskConfiguration.getRemoteBaseUrl());

        }
        return null;
    }

    private RequestBehaviour getLoginBehaviour(RemoteProductType productType, final TaskConfiguration taskConfiguration)
    {
        RequestBehaviour loginBehaviour;
        if (taskConfiguration.useAtlassianId())
        {
            loginBehaviour = new AtlassianIdLoginRequestBehaviour();
        }
        else if (isOnDemand(taskConfiguration))
        {
            loginBehaviour = new OnDemandLoginRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
        }
        else
        {
            // Get login behaviour by product.
            if (productType == RemoteProductType.CONFLUENCE)
            {
                loginBehaviour = new ConfluenceLoginRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
            }
            else if (productType == RemoteProductType.JIRA)
            {
                loginBehaviour = new JiraLoginRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
            }
            else if (productType == RemoteProductType.BAMBOO)
            {
                loginBehaviour = new BambooLoginRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
            }
            else if (productType == RemoteProductType.FECRU)
            {
                loginBehaviour = new FeCruLoginRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
            }
            else if (productType == RemoteProductType.STASH)
            {
                loginBehaviour = new StashLoginRequestBehaviour(taskConfiguration.getRemoteBaseUrl());
            }
            else
            {
                // Unknown login behaviour
                throw new RuntimeException(String.format("Unknown RemoteProductType %s", productType));
            }
        }
        return loginBehaviour;
    }
}
