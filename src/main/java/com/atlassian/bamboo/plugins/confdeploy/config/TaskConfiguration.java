package com.atlassian.bamboo.plugins.confdeploy.config;

import com.atlassian.fugue.Maybe;

import java.io.File;

/**
 * Encapsulates the {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask} configuration values, distilled down
 * to the minimum required to actually execute the task.
 */
public interface TaskConfiguration
{
    /**
     * Identifies which product is being deployed to.
     */
    public Maybe<RemoteProductType> getProduct();

    /**
     * The physical compiled artifact on the file system that will be uploaded to the remote Atlassian application.
     * @return A {@link java.io.File} object reference to the artifact.
     */
    public File getPluginJar();

    /**
     * The configured Base URL of the remote Atlassian application that will be uploaded to.
     */
    public String getRemoteBaseUrl();

    /**
     * The configured username that will be used to authenticate with the remote Atlassian application. The user must
     * have System Administration privileges in the remote application in order to be able to upload and install
     * plugins.
     *
     * @see #getPassword()
     */
    public String getUsername();

    /**
     * The password for the configured {@link #getUsername()}} user.
     *
     * @see #getUsername()
     */
    public String getPassword();

    /**
     * Determines whether or not the {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask} will authenticate against the remote Atlassian application
     * using Atlassian ID, instead of the product's own login mechanism. This allows the AutoDeployTask to function
     * correctly for systems that are secured using Atlassian ID, such as https://jira.atlassian.com and
     * https://confluence.atlassian.com
     */
    public boolean useAtlassianId();

    /**
     * Determines whether or not the {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask} will use the Atlassian ID WebSudo mechanism for obtaining
     * a Secure Administrator Session, instead of the product's own WebSudo mechanism. This must be kept as a separate
     * option from the {@link #useAtlassianId()} setting, as it is possible for an Atlassian application to be
     * configured with Atlassian ID for login, but to be kept using its own internal WebSudo mechanism.
     *
     * Atlassian ID WebSudo is going through a staged roll-out to Atlassian systems at the time of writing this, so
     * perhaps in the future there will no longer be a need to support this option.
     */
    public boolean useAtlassianIdWebSudo();

    /**
     * The configured username that will be used to authenticate against Atlassian ID, if the {@link #useAtlassianId()}
     * setting is enabled. This must be a separate configuration option from the {@link #getUsername()} setting because
     * two different sets of usernames and passwords may be required if {@link #useAtlassianId()} is {@code true} and
     * {@link #useAtlassianIdWebSudo()} is {@code false}.
     *
     * @see #getAtlassianIdPassword()
     */
    public String getAtlassianIdUsername();

    /**
     * The password for the configured {@link #getAtlassianIdUsername()}} user.
     *
     * @see #getAtlassianIdUsername()
     */
    public String getAtlassianIdPassword();

    /**
     * The Base URL of the Atlassian ID system to authenticate against. The default is https://id.atlassian.com, but
     * this value is configurable in case it is required to authenticate against a staging or development environment.
     */
    public String getAtlassianIdBaseUrl();

    /**
     * The application nickname that Atlassian ID associates with the application being deployed to. This is only
     * necessary if {@link #useAtlassianIdWebSudo()} is enabled. For example, "jac" for https://jira.atlassian.com
     * and "sac" for https://support.atlassian.com
     */
    public String getAtlassianIdAppName();

    /**
     * Determines whether or not to fail the build if the remote Atlassian application's server certificate cannot be
     * verified. This setting may be useful when deploying to an application running with a self-signed certificate,
     * for example.
     */
    public boolean allowSSLCertificateErrors();

    /**
     * If this setting is enabled, the {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask} will execute when the currently-running build is a
     * branch-based build.
     */
    public boolean allowBranchBuildExecution();

    /**
     * Determines whether or not to log all incoming and outgoing HTTP Traffic to the build log, which can be used to
     * diagnose build failures.
     */
    public boolean getEnableTrafficLogging();

    /**
     * Returns the number of seconds the {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask} for the remote application's plugin manager to report
     * that the uploaded plugin was successfully installed. If the timeout is exceeded, the task is failed.
     */
    public int getPluginInstallationTimeout();
}
