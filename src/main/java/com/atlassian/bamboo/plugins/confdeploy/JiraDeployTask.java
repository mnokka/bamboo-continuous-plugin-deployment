package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;

/**
 * @deprecated Use {@link com.atlassian.bamboo.plugins.deploy.PluginDeployTask#} instead. Since v3.0.
 */
@Deprecated
public class JiraDeployTask extends AutoDeployTask
{
    @Override
    public RemoteProductType getProductType(TaskConfiguration configuration)
    {
        return RemoteProductType.JIRA;
    }
}
