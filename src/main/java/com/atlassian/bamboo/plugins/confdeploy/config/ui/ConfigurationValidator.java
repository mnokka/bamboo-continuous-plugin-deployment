package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.utils.error.ErrorCollection;

/**
 * Contains the business logic that determines if a set of configuration values entered by the user is valid for the
 * {@link com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask}.
 */
public interface ConfigurationValidator
{
    public void validate(TaskParametersMap params, final ErrorCollection errors);
}
