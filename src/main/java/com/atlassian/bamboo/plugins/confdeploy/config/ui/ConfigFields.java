package com.atlassian.bamboo.plugins.confdeploy.config.ui;

/**
 *
 */
public class ConfigFields
{
    public static final String PRODUCT_TYPE = "bcpd.config.productType";
    public static final String PLUGIN_ARTIFACT = "confDeployJar";
    public static final String BASE_URL = "confDeployURL";
    public static final String USERNAME = "confDeployUsername";
    public static final String USE_PASSWORD_VARIABLE = "confDeployPasswordVariableCheck";
    public static final String PASSWORD = "confDeployPassword";
    public static final String PASSWORD_ENCRYPTION_KEY = "confDeployKey";
    public static final String PASSWORD_VARIABLE = "confDeployPasswordVariable";
    public static final String USE_ATLASSIAN_ID = "useAtlassianId";
    public static final String ATLASSIAN_ID_USERNAME = "atlassianIdUsername";
    public static final String ATLASSIAN_ID_PASSWORD = "atlassianIdPassword";
    public static final String ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY = "atlassianIdKey";
    public static final String USE_ATLASSIAN_ID_PASSWORD_VARIABLE = "atlassianIdPasswordVariableCheck";
    public static final String ATLASSIAN_ID_PASSWORD_VARIABLE = "atlassianIdPasswordVariable";
    public static final String USE_ATLASSIAN_ID_WEBSUDO = "useAtlassianIdWebSudo";
    public static final String ATLASSIAN_ID_APP_NAME = "atlassianIdAppName";
    public static final String ATLASSIAN_ID_BASE_URL = "atlassianIdBaseURL";
    public static final String ALLOW_BRANCH_BUILDS = "deployBranchEnabled";
    public static final String ALLOW_SSL_ERRORS = "certificateCheckDisabled";
    public static final String ENABLE_TRAFFIC_LOGGING = "enableTrafficLogging";
    public static final String PLUGIN_INSTALLATION_TIMEOUT = "pluginInstallationTimeout";

}
