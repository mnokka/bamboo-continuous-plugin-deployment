package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.PollingRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.HttpClientWrapper;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.fugue.Either;
import com.atlassian.util.concurrent.Nullable;
import com.atlassian.util.concurrent.Timeout;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * {@inheritDoc}
 */
class DefaultUploadClient implements UploadClient
{
    private final RequestBehaviour loginStrategy;
    @Nullable private final RequestBehaviour permissionCheckStrategy;
    @Nullable private final RequestBehaviour webSudoStrategy;
    private final RequestBehaviour upmTokenStrategy;
    private final RequestBehaviour pluginUploadStrategy;
    private final PollingRequestBehaviour uploadSuccessCheckStrategy;
    @Nullable private final RequestBehaviour upmSelfUpdateStrategy;
    @Nullable private final PollingRequestBehaviour upmSelfUpdateCheckStrategy;

    private final HttpClientWrapper client;
    private final BuildLogger buildLogger;

    /**
     * Construct using {@link UploadClientFactory}.
     *
     */
    DefaultUploadClient(final RequestBehaviour loginStrategy,
                        @Nullable final RequestBehaviour permissionCheckStrategy,
                        @Nullable final RequestBehaviour webSudoStrategy,
                        final RequestBehaviour upmTokenStrategy, final RequestBehaviour pluginUploadStrategy,
                        final PollingRequestBehaviour uploadSuccessCheckStrategy, @Nullable final RequestBehaviour upmSelfUpdateStrategy,
                        @Nullable final PollingRequestBehaviour upmSelfUpdateCheckStrategy,
                        final BuildLogger buildLogger, final HttpClientWrapper client)
    {
        this.loginStrategy = loginStrategy;
        this.permissionCheckStrategy = permissionCheckStrategy;
        this.webSudoStrategy = webSudoStrategy;
        this.upmTokenStrategy = upmTokenStrategy;
        this.pluginUploadStrategy = pluginUploadStrategy;
        this.uploadSuccessCheckStrategy = uploadSuccessCheckStrategy;
        this.upmSelfUpdateStrategy = upmSelfUpdateStrategy;
        this.upmSelfUpdateCheckStrategy = upmSelfUpdateCheckStrategy;

        this.buildLogger = buildLogger;
        this.client = client;
    }

    @Override
    public Result deploy(final TaskConfiguration taskConfiguration) throws TaskException
    {
        Map<String, Object> requestContext = new HashMap<String, Object>();

        // Login
        Result loginResult = executeRequest(loginStrategy, taskConfiguration, requestContext);
        if (!loginResult.isSuccess())
            return loginResult;

        // Permission check (may be null if not supported by product type)
        if (permissionCheckStrategy != null)
        {
            Result permissionCheckResult = executeRequest(permissionCheckStrategy, taskConfiguration, requestContext);
            if (!permissionCheckResult.isSuccess())
                return permissionCheckResult;
        }

        // WebSudo (may be null if WebSudo is not supported by the target host)
        if (webSudoStrategy != null)
        {
            Result webSudoResult = executeRequest(webSudoStrategy, taskConfiguration, requestContext);
            if (!webSudoResult.isSuccess())
                return webSudoResult;
        }

        // Get UPM XSRF Token
        Result getTokenResult = executeRequest(upmTokenStrategy, taskConfiguration, requestContext);
        if (!getTokenResult.isSuccess())
            return getTokenResult;

        String upmToken = (String)getTokenResult.getContext();
        requestContext.put("upmToken", upmToken);

        // Plugin upload
        Result uploadResult = executeRequest(pluginUploadStrategy, taskConfiguration, requestContext);
        if (!uploadResult.isSuccess())
            return uploadResult;

        JSONObject uploadResponse = (JSONObject)uploadResult.getContext();
        requestContext.put("responseEntity", uploadResponse);

        // Poll for success.
        Result pollResult = executePollingRequest(uploadSuccessCheckStrategy, taskConfiguration, requestContext, Timeout.getMillisTimeout(taskConfiguration.getPluginInstallationTimeout(), TimeUnit.SECONDS));
        if (!pollResult.isSuccess())
            return pollResult;

        // Installing UPM through UPM is a bit more complicated. It has to be special-cased as a result.
        // (may be null if this is not the UPM jar)
        if (upmSelfUpdateStrategy != null && upmSelfUpdateCheckStrategy != null)
        {
            // Terrible, but the self-update plugin doesn't appear to always be ready by the time we call it.
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                throw new TaskException("Error while waiting to trigger UPM self-update", e);
            }

            buildLogger.addBuildLogEntry(String.format("Contacting %s to trigger UPM self-update.", taskConfiguration.getRemoteBaseUrl()));
            Result triggerSelfUpdateResult = executeRequest(upmSelfUpdateStrategy, taskConfiguration, requestContext);
            if (!triggerSelfUpdateResult.isSuccess())
                return triggerSelfUpdateResult;

            Result selfUpdateResult = executePollingRequest(upmSelfUpdateCheckStrategy, taskConfiguration, requestContext, Timeout.getMillisTimeout(taskConfiguration.getPluginInstallationTimeout(), TimeUnit.SECONDS));
            if (!selfUpdateResult.isSuccess())
                return selfUpdateResult;
        }

        return Result.success("Plugin upload completed successfully. Nice going, mate!");
    }

    private Result executePollingRequest(final PollingRequestBehaviour requestBehaviour, final TaskConfiguration taskConfiguration, final Map<String, Object> requestContext, final Timeout timeout)
    {
        Either<HttpUriRequest, Failure> maybeRequest = requestBehaviour.getRequest(taskConfiguration, requestContext);
        if (maybeRequest.isRight())
        {
            // Request construction failed.
            Failure failure = maybeRequest.right().get();
            buildLogger.addErrorLogEntry(failure.getMessage(), failure.getContext() instanceof Exception ? (Exception)failure.getContext() : null);
        }

        Result pollOutcome = client.poll(maybeRequest.left().get(), requestBehaviour, timeout);
        if (!pollOutcome.isSuccess())
        {
            buildLogger.addErrorLogEntry(pollOutcome.getMessage(), pollOutcome.getContext() instanceof Exception ? (Exception)pollOutcome.getContext() : null);
        }
        else
        {
            buildLogger.addBuildLogEntry(pollOutcome.getMessage());

        }
        return pollOutcome;
    }

    private Result executeRequest(final RequestBehaviour requestBehaviour, final TaskConfiguration taskConfiguration, final Map<String, Object> requestContext)
    {
        Either<HttpUriRequest, Failure> maybeRequest = requestBehaviour.getRequest(taskConfiguration, requestContext);
        if (maybeRequest.isRight())
        {
            // Request construction failed.
            Failure failure = maybeRequest.right().get();
            buildLogger.addErrorLogEntry(failure.getMessage(), failure.getContext() instanceof Exception ? (Exception)failure.getContext() : null);
        }

        Result responseOutcome = client.execute(maybeRequest.left().get(), requestBehaviour);
        if (!responseOutcome.isSuccess())
        {
            buildLogger.addErrorLogEntry(responseOutcome.getMessage(), responseOutcome.getContext() instanceof Exception ? (Exception)responseOutcome.getContext() : null);
        }
        else
        {
            buildLogger.addBuildLogEntry(responseOutcome.getMessage());

        }
        return responseOutcome;
    }

    // These getters are basically just for unit-testing.

    public RequestBehaviour getLoginStrategy()
    {
        return this.loginStrategy;
    }

    public RequestBehaviour getWebSudoStrategy()
    {
        return this.webSudoStrategy;
    }

    public RequestBehaviour getUpmTokenStrategy()
    {
        return this.upmTokenStrategy;
    }

    public RequestBehaviour getPluginUploadStrategy()
    {
        return this.pluginUploadStrategy;
    }

    public PollingRequestBehaviour getUploadSuccessCheckStrategy()
    {
        return this.uploadSuccessCheckStrategy;
    }

    public RequestBehaviour getUpmSelfUpdateStrategy()
    {
        return this.upmSelfUpdateStrategy;
    }

    public PollingRequestBehaviour getUpmSelfUpdateCheckStrategy()
    {
        return this.upmSelfUpdateCheckStrategy;
    }


}
