package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.deployments.DeploymentTaskContextHelper;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plan.artifact.ImmutableArtifactSubscription;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugin.ArtifactDownloaderTaskConfigurationHelper;
import com.atlassian.bamboo.plugin.BambooPluginUtils;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.task.TaskContextHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.user.BambooAuthenticationContext;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.util.concurrent.NotNull;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * {@inheritDoc}
 */
public class DefaultArtifactSubscriber implements ArtifactSubscriber
{
    private static final long ALL_ARTIFACTS_ID = -1L;

    private final ArtifactDefinitionManager artifactDefinitionManager;
    private final CachedPlanManager cachedPlanManager;
    private final BambooAuthenticationContext bambooAuthenticationContext;

    public DefaultArtifactSubscriber(final ArtifactDefinitionManager artifactDefinitionManager,
                                     final CachedPlanManager cachedPlanManager,
                                     final BambooAuthenticationContext bambooAuthenticationContext)
    {
        this.artifactDefinitionManager = artifactDefinitionManager;
        this.cachedPlanManager = cachedPlanManager;
        this.bambooAuthenticationContext = bambooAuthenticationContext;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterable<SubscribedArtifact> getSubscriptions(final Map<String, Object> taskContext)
    {
        if (TaskContextHelper.isDeploymentMode(taskContext))
        {
            // Deployment Project
            Environment environment = DeploymentTaskContextHelper.getEnvironment(taskContext);
            assert environment != null;

            return getDownloadedArtifacts(environment.getTaskDefinitions());
        }
        else
        {
            // Build Plan
            ImmutableJob job = Narrow.downTo(TaskContextHelper.getPlan(taskContext), ImmutableJob.class);
            assert job != null;

            return Iterables.concat(getSubscriptionsFromJob(job), getDownloadedArtifacts(job.getBuildDefinition().getTaskDefinitions()));
        }
    }

    /**
     * Retrieves artifacts that are subscribed to as an explicit dependency at the Job level.
     */
    private Iterable<SubscribedArtifact> getSubscriptionsFromJob(@NotNull ImmutableJob job)
    {
        return Iterables.transform(job.getArtifactSubscriptions(), new Function<ImmutableArtifactSubscription, SubscribedArtifact>()
        {
            @Override
            public SubscribedArtifact apply(ImmutableArtifactSubscription subscription)
            {
                // TODO: Don't depend on AvailableArtifact string logic here.
                return new SubscribedArtifact(AvailableArtifact.from(subscription).toString(), bambooAuthenticationContext.getI18NBean().getText("bcpd.config.form.artifact.subscription.artifacts"), subscription.getName());
            }
        });
    }

    /**
     * Retrieves artifacts that are subscribed to by virtue of an Artifact Download task in the same Job as this task.
     */
    private Iterable<SubscribedArtifact> getDownloadedArtifacts(@NotNull List<TaskDefinition> taskDefinitions)
    {
        List<SubscribedArtifact> artifacts = new ArrayList<SubscribedArtifact>();

        // Find all the enabled Artifact Download tasks
        Iterable<TaskDefinition> downloadTasks = Iterables.filter(taskDefinitions, new Predicate<TaskDefinition>()
        {
            @Override
            public boolean apply(TaskDefinition task)
            {
                return task.getPluginKey().equals(BambooPluginUtils.ARTIFACT_DOWNLOAD_TASK_MODULE_KEY) && task.isEnabled();
            }
        });

        // Provide a subscription reference for each artifact provided by each download task
        for (TaskDefinition task : downloadTasks)
        {
            final Map<String, String> taskConfiguration = task.getConfiguration();

            // Give a descriptive group name to artifacts from this task
            final String groupName = getGroupName(taskConfiguration);

            // Process each artifact
            for (String artifactKey : ArtifactDownloaderTaskConfigurationHelper.getArtifactKeys(taskConfiguration))
            {
                final long artifactId = Long.valueOf(taskConfiguration.get(artifactKey));
                final int transferId = ArtifactDownloaderTaskConfigurationHelper.getIndexFromKey(artifactKey);

                boolean isAllArtifacts = artifactId == ALL_ARTIFACTS_ID;
                if (isAllArtifacts)
                {
                    // HACK: I HAVE NO IDEA WHAT I'M DOING.

                    String sourcePlanKey = ArtifactDownloaderTaskConfigurationHelper.getSourcePlanKey(taskConfiguration);
                    ImmutablePlan sourcePlan = cachedPlanManager.getPlanByKey(PlanKeys.getPlanKey(sourcePlanKey));
                    ImmutableChain chain = Narrow.to(sourcePlan, ImmutableChain.class);

                    List<ArtifactDefinition> sharedArtifactsByChain = artifactDefinitionManager.findSharedArtifactsByChain(chain);
                    for (ArtifactDefinition d : sharedArtifactsByChain)
                    {
                        artifacts.add(new SubscribedArtifact(AvailableArtifact.fromTransferTask(d.getId(), task, transferId, d.getName()).toString(),
                                                             groupName,
                                                             getArtifactName(d)));
                    }
                }
                else
                {
                    if (!taskConfiguration.containsKey(artifactKey))
                        continue;


                    final ArtifactDefinition artifactDefinition = artifactDefinitionManager.findArtifactDefinition(artifactId);
                    if (artifactDefinition == null)
                        continue;

                    artifacts.add(
                            new SubscribedArtifact(
                                    // TODO: Don't depend on AvailableArtifact string logic here.
                                    AvailableArtifact.fromTransferTask(artifactDefinition.getId(), task, transferId, artifactDefinition.getName()).toString(),
                                    groupName,
                                    getArtifactName(artifactDefinition)
                            )
                    );
                }
            }
        }

        return artifacts;
    }

    private String getGroupName(Map<String, String> taskConfiguration)
    {
        final String sourcePlanKey = ArtifactDownloaderTaskConfigurationHelper.getSourcePlanKey(taskConfiguration);
        final ImmutablePlan plan = cachedPlanManager.getPlanByKey(PlanKeys.getPlanKey(sourcePlanKey));
        return bambooAuthenticationContext.getI18NBean().getText("bcpd.config.form.artifact.plan.artifacts", new String[]{plan.getName()});
    }

    private String getArtifactName(final ArtifactDefinition artifact)
    {
        return bambooAuthenticationContext.getI18NBean().getText("bcpd.config.form.artifact.plan.one.artifact", new String[]{artifact.getProducerJob().getKey(), artifact.getName()});
    }

}
