package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.Lists;
import org.apache.commons.lang.Validate;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Knows how to dance WebSudo with Atlassian ID. Unlike product-based WebSudo implementations, Atlassian ID requires the
 * "application" nickname to be supplied with the request, so that it knows which product instance is being targeted
 * (eg. 'jac' for 'jira.atlassian.com' and 'sac' for 'support.atlassian.com').
 */
public class AtlassianIdWebSudoRequestBehaviour implements RequestBehaviour
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        Validate.notEmpty(configuration.getAtlassianIdBaseUrl());
        Validate.notEmpty(configuration.getAtlassianIdAppName());
        Validate.notEmpty(configuration.getAtlassianIdUsername());
        Validate.notEmpty(configuration.getAtlassianIdPassword());

        final String appNameEncoded;
        try
        {
            appNameEncoded = URLEncoder.encode(configuration.getAtlassianIdAppName(), "utf-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
        String requestUrl = UrlUtils.join(configuration.getAtlassianIdBaseUrl(), "/id/websudo.action?application=" + appNameEncoded);

        HttpPost request = new HttpPost(requestUrl);
        try
        {
            request.setEntity(new UrlEncodedFormEntity(
                    Lists.<NameValuePair>newArrayList(
                            new BasicNameValuePair("username", configuration.getAtlassianIdUsername()),
                            new BasicNameValuePair("password", configuration.getAtlassianIdPassword()),
                            new BasicNameValuePair("websudo", "false")
                    )
            ));
        }
        catch (UnsupportedEncodingException e)
        {
            return Either.right(Result.failure("Failed to encode UTF-8 String - Java, you suck.", e));
        }

        return Either.<HttpUriRequest, Failure>left(request);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode == 302)
            {
                // Success.
                return Result.success("Atlassian ID WebSudo complete!");
            }
            else
            {
                return Result.failure("Atlassian ID WebSudo failed. HTTP status " + responseCode + " was returned (note: HTTP 200 OK is a failure in this case");
            }
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
