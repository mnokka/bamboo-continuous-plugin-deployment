package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import com.google.common.base.Function;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;

import java.util.Map;

/**
 * Tastes like {@link RequestBehaviour}, but the request is destined to be repeatable.
 */
public interface PollingRequestBehaviour extends Function<HttpResponse, Either<HttpUriRequest, Result>>
{
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext);
}
