package com.atlassian.bamboo.plugins.confdeploy;

/**
 * Dammit, Java. I hate you.
 *
 * If I were a better developer, I would have come up with a better way to do this.
 */
public class UrlUtils
{
    /**
     * Combines a Base URL with a path URL component. The Base URL may already have a path component, in which cause the
     * supplied path component will be appended to it.
     *
     * @param root The Base URL, eg. "https://extranet.atlassian.com/jira" or "http://confluence.atlassian.com"
     * @param path The path relative to the Base URL, eg. "/rest/1.0/plugins" or "dologin.action"
     * @return The combined URL.
     */
    public static String join(String root, String path)
    {
        if (root.endsWith("/"))
        {
            if (path.startsWith("/"))
            {
                return root + path.substring(1);
            }
            return root + path;
        }
        else
        {
            if (path.startsWith("/"))
            {
                return root + path;
            }
            return root + "/" + path;
        }
    }

    /**
     * Returns the 'scheme', 'host' and 'port' components of the URL only.
     */
    public static String getHostUrl(String url)
    {
        int firstSlashPos;
        if (url.startsWith("http://"))
        {
            firstSlashPos = url.indexOf("/", "http://".length());
        }
        else if (url.startsWith("https://"))
        {
            firstSlashPos = url.indexOf("/", "https://".length());
        }
        else
        {
            throw new UnsupportedOperationException("Sorry, this method cannot manipulated non-HTTP(s) URLs because Joe is a crappy programmer.");
        }
        if (firstSlashPos >= 0)
        {
            return url.substring(0, firstSlashPos);
        }
        // No changes required
        return url;
    }
}
