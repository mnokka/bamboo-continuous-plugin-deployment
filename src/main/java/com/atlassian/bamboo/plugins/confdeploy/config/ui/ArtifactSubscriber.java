package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import java.util.Map;

/**
 * Retrieves all available artifact subscriptions available in the context of a single task.
 */
public interface ArtifactSubscriber
{
    /**
     * Retrieve all subscribed artifacts for the supplied task context.
     */
    Iterable<SubscribedArtifact> getSubscriptions(final Map<String, Object> taskContext);
}
