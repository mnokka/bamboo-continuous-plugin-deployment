package com.atlassian.bamboo.plugins.confdeploy.config;

import javax.annotation.Nonnull;

/**
 * Enumerates the remote products that this plugin is capable of deploying plugins to.
 */
public enum RemoteProductType
{
    CONFLUENCE("bcpd.product.confluence", true), JIRA("bcpd.product.jira", true), BAMBOO("bcpd.product.bamboo", false), FECRU("bcpd.product.fecru", false), STASH("bcpd.product.stash", false);

    private final String displayKey;
    private final boolean supportsWebSudo;

    RemoteProductType(final String displayKey, final boolean supportsWebSudo)
    {
        this.displayKey = displayKey;
        this.supportsWebSudo = supportsWebSudo;
    }

    public String getProductKey()
    {
        return displayKey;
    }

    public boolean getSupportsWebSudo()
    {
        return supportsWebSudo;
    }

    public static RemoteProductType fromKey(@Nonnull final String productKey)
    {
        for (RemoteProductType type : RemoteProductType.values())
        {
            if (type.getProductKey().equals(productKey))
                return type;
        }

        return null;
    }
}