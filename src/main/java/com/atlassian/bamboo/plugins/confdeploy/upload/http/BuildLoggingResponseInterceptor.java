package com.atlassian.bamboo.plugins.confdeploy.upload.http;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.http.*;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * {@link org.apache.http.HttpResponseInterceptor} that dumps {@link org.apache.http.HttpResponse} information to the Bamboo build log.
 */
public class BuildLoggingResponseInterceptor implements HttpResponseInterceptor
{
    private final BuildLogger buildLogger;
    private final TaskConfiguration taskConfiguration;

    public BuildLoggingResponseInterceptor(final BuildLogger buildLogger, final TaskConfiguration taskConfiguration)
    {
        this.buildLogger = buildLogger;
        this.taskConfiguration = taskConfiguration;
    }

    @Override
    public void process(HttpResponse response, HttpContext context) throws HttpException, IOException
    {
        buildLogger.addBuildLogEntry("**** Incoming Response Debug Log ****");
        StatusLine statusLine = response.getStatusLine();
        logWithNoPasswords("< " + statusLine.toString());
        for (Header h : response.getAllHeaders())
        {
            logWithNoPasswords("< " + h.toString());
        }
        buildLogger.addBuildLogEntry("< ");
        HttpEntity responseEntity = response.getEntity();
        if (responseEntity != null)
        {
            if (!responseEntity.isRepeatable())
            {
                responseEntity = new RepeatableHttpEntity(responseEntity);
                response.setEntity(responseEntity);
            }
            logWithNoPasswords("< " + EntityUtils.toString(responseEntity));
        }
        buildLogger.addBuildLogEntry("*************************************");
    }

    private void logWithNoPasswords(String logMessage)
    {
        if (StringUtils.isNotBlank(taskConfiguration.getPassword()) && logMessage.contains(taskConfiguration.getPassword()))
        {
            logMessage = logMessage.replace(taskConfiguration.getPassword(), "******");
        }

        if (StringUtils.isNotBlank(taskConfiguration.getAtlassianIdPassword()) && logMessage.contains(taskConfiguration.getAtlassianIdPassword()))
        {
            logMessage = logMessage.replace(taskConfiguration.getAtlassianIdPassword(), "******");
        }

        buildLogger.addBuildLogEntry(logMessage);
    }
}
