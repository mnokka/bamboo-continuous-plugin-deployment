package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ProductLoginRequestBehaviour;

/**
 * Use me for logging in to Bamboo!
 */
public class BambooLoginRequestBehaviour extends ProductLoginRequestBehaviour
{
    public BambooLoginRequestBehaviour(final String baseUrl)
    {
        super(UrlUtils.join(baseUrl, "/userlogin.action"), "os_username", "os_password");
    }
}
