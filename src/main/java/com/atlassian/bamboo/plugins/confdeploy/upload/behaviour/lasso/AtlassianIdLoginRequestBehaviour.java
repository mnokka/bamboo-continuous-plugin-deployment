package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.commons.lang.Validate;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Knows how to login to a system secured with Atlassian ID. The Atlassian ID host URL is configurable, so can be used
 * to log in to the Atlassian ID production system, or to staging and dev environments.
 */
public class AtlassianIdLoginRequestBehaviour implements RequestBehaviour
{
    private static final String LOGIN_RELATIVE_URL = "/id/rest/login";

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(final TaskConfiguration configuration, final Map<String, Object> requestContext)
    {
        Validate.notEmpty(configuration.getAtlassianIdUsername(), "Atlassian ID Username must be specified to authenticate with Atlassian ID.");
        Validate.notEmpty(configuration.getAtlassianIdPassword(), "Atlassian ID Password must be specified to authenticate with Atlassian ID.");
        Validate.notEmpty(configuration.getAtlassianIdBaseUrl(), "Atlassian ID Base URL must be specified to authenticate with Atlassian ID.");

        String loginUrl = UrlUtils.join(configuration.getAtlassianIdBaseUrl(), LOGIN_RELATIVE_URL);

        HttpPost loginRequest = new HttpPost(loginUrl);
        loginRequest.addHeader("Accept", "application/json");
        try
        {
            JSONObject j = new JSONObject();
            j.put("username", configuration.getAtlassianIdUsername());
            j.put("password", configuration.getAtlassianIdPassword());
            loginRequest.setEntity(new StringEntity(j.toString(), "application/json", "utf-8"));
        }
        catch (JSONException e)
        {
            return Either.right(Result.failure("Failed to construct JSON request body: " + e.getMessage(), e));
        }
        catch (UnsupportedEncodingException e)
        {
            return Either.right(Result.failure("Failed to encode UTF-8 String - Java, you suck.", e));
        }

        return Either.<HttpUriRequest, Failure>left(loginRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 403)
            {
                // Authorisation failed
                return Result.failure("Sorry, your Atlassian ID credentials were incorrect.");
            }
            else if (statusCode == 200)
            {
                // OK!
                return Result.success("Login to Atlassian ID was successful.");
            }
            else
            {
                // Some other error!
                return Result.failure("Atlassian ID login failed. HTTP Status code " + statusCode + " returned.");
            }
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }
    }
}
