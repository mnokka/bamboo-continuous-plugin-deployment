package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskContext;
import com.atlassian.bamboo.deployments.execution.DeploymentTaskType;
import com.atlassian.bamboo.plugins.confdeploy.config.*;
import com.atlassian.bamboo.plugins.confdeploy.upload.DefaultUploadClientFactory;
import com.atlassian.bamboo.plugins.confdeploy.upload.UploadClient;
import com.atlassian.bamboo.plugins.confdeploy.upload.UploadClientFactory;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.fugue.Either;
import com.atlassian.tunnel.utils.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.util.Properties;

public abstract class AutoDeployTask implements TaskType, DeploymentTaskType
{
    public static final int DEFAULT_PLUGIN_INSTALLATION_TIMEOUT = 90;

    private CustomVariableContext variableContext;

    public AutoDeployTask()
    {
    }

    public abstract RemoteProductType getProductType(TaskConfiguration configuration);

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();
        ArtifactRetriever artifactRetriever = new JobArtifactRetriever(taskContext, variableContext);
        final TaskConfigurationFactory taskConfigurationFactory = new DefaultTaskConfigurationFactory(artifactRetriever);

        Either<TaskConfiguration,Failure> maybeConfig = taskConfigurationFactory.getTaskConfiguration(taskContext.getConfigurationMap());

        if (maybeConfig.isRight())
        {
            final Result failure = maybeConfig.right().get();
            buildLogger.addBuildLogEntry(failure.getMessage());
            return TaskResultBuilder.newBuilder(taskContext).failed().build();
        }
        final TaskConfiguration config = maybeConfig.left().get();

        // Only run for branch builds if the user has asked to do so
        BuildContext buildContext = taskContext.getBuildContext();
        if (!config.allowBranchBuildExecution() && buildContext.isBranch())
        {
            buildLogger.addBuildLogEntry("Plugin deployment task has been disabled for branch builds.");
            return TaskResultBuilder.newBuilder(taskContext).success().build();
        }
        return run(taskContext, config, buildLogger);
    }
    
    @Override
    @NotNull
    public TaskResult execute(@NotNull DeploymentTaskContext deploymentContext) throws TaskException
    {
        final BuildLogger buildLogger = deploymentContext.getBuildLogger();
        ArtifactRetriever artifactRetriever = new DeploymentArtifactRetriever(deploymentContext, variableContext);
        final TaskConfigurationFactory taskConfigurationFactory = new DefaultTaskConfigurationFactory(artifactRetriever);

        Either<TaskConfiguration,Failure> maybeConfig = taskConfigurationFactory.getTaskConfiguration(deploymentContext.getConfigurationMap());
        if (maybeConfig.isRight())
        {
            final Result failure = maybeConfig.right().get();
            buildLogger.addBuildLogEntry(failure.getMessage());
            return TaskResultBuilder.newBuilder(deploymentContext).failed().build();
        }

        return run(deploymentContext, maybeConfig.left().get(), buildLogger);
    }

    private TaskResult run(final CommonTaskContext context, final TaskConfiguration config, final BuildLogger buildLogger) throws TaskException
    {
        Deprecated annotation = this.getClass().getAnnotation(Deprecated.class);
        if (annotation != null)
        {
            buildLogger.addErrorLogEntry("This task is deprecated and will be removed in a future version of this plugin. You should edit your task configuration and replace this task with the new 'Deploy Plugin' task.");
        }

        InputStream pomProperties = null;
        try
        {
            pomProperties = this.getClass().getClassLoader().getResourceAsStream("META-INF/maven/com.atlassian.bamboo.plugins.deploy/continuous-plugin-deployment/pom.properties");
            Properties mavenProperties = new Properties();
            mavenProperties.load(pomProperties);

            Object versionObj = mavenProperties.get("version");
            if (versionObj == null)
            {
                buildLogger.addBuildLogEntry("Starting plugin deployment task (version unknown - is this a custom build??");
            }
            else
            {
                buildLogger.addBuildLogEntry("Starting plugin deployment task (v. " + versionObj.toString() + ")");

            }
        }
        catch (Exception e)
        {
            buildLogger.addBuildLogEntry("Starting plugin deployment task (failed to load version: " + e.getMessage() + ")");
        }
        finally
        {
            IOUtils.closeQuietly(pomProperties);
        }
        buildLogger.addBuildLogEntry(String.format("Deploying %s to %s at %s", config.getPluginJar().getName(), getProductType(config), config.getRemoteBaseUrl()));

        // Invoke the upload client
        UploadClientFactory uploadClientFactory = new DefaultUploadClientFactory();
        UploadClient uploadClient = uploadClientFactory.getUploadClient(getProductType(config), config, buildLogger);

        Result uploadResult = uploadClient.deploy(config);
        buildLogger.addBuildLogEntry(uploadResult.getMessage());
        if (uploadResult.isSuccess())
            return TaskResultBuilder.newBuilder(context).success().build();

        return TaskResultBuilder.newBuilder(context).failed().build();
    }

    // TODO: Can this be injected in the constructor?
    public void setVariableContext(CustomVariableContext variableContext)
    {
        this.variableContext = variableContext;
    }
}
