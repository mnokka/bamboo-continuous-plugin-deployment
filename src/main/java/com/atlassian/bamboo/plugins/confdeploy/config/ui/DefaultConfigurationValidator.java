package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.plugins.confdeploy.AvailableArtifact;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.i18n.I18nBean;
import org.apache.commons.lang.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * {@inheritDoc}
 */
public class DefaultConfigurationValidator implements ConfigurationValidator
{
    private final ArtifactDefinitionManager artifactDefinitionManager;
    private final I18nBean i18nBean;

    public DefaultConfigurationValidator(final I18nBean i18nBean, final ArtifactDefinitionManager artifactDefinitionManager)
    {
        this.i18nBean = i18nBean;
        this.artifactDefinitionManager = artifactDefinitionManager;
    }

    private void validatePassword(final boolean usePasswordVariable, final String passwordVariable, final String password, final ErrorCollection errors)
    {
        if (usePasswordVariable)
        {
            if (StringUtils.isBlank(passwordVariable))
            {
                errors.addErrorMessage(i18nBean.getText("bcpd.config.form.password.error.required"));
            }
            else if (!isBambooVariable(passwordVariable))
            {
                errors.addErrorMessage(i18nBean.getText("bcpd.config.form.password.error.invalid"));
            }
        }
        else
        {
            if (StringUtils.isBlank(password))
            {
                errors.addErrorMessage(i18nBean.getText("bcpd.config.form.password.error.required"));
            }
        }
    }

    private boolean isBambooVariable(final String fieldValue)
    {
        // HACK: This is not 100% accurate, but hopefully good enough?
        return StringUtils.isNotBlank(fieldValue) && fieldValue.startsWith("${bamboo.");
    }

    private boolean validateNotBlank(final String fieldName, final String fieldValue, final String errorKey, final ErrorCollection errors)
    {
        if (StringUtils.isBlank(fieldValue))
        {
            if (StringUtils.isBlank(fieldName))
            {
                errors.addErrorMessage(i18nBean.getText(errorKey));
            }
            else
            {
                errors.addError(fieldName, i18nBean.getText(errorKey));
            }
            return false;
        }

        return true;
    }

    private boolean validateIsUrl(final String fieldName, final String fieldValue, final String errorKey, final ErrorCollection errors)
    {
        // Bamboo variables are allowed in URL parameters.
        if (isBambooVariable(fieldValue))
            return true;

        try
        {
            new URL(fieldValue);
            return true;
        }
        catch (MalformedURLException e)
        {
            if (StringUtils.isBlank(fieldName))
            {
                errors.addErrorMessage(i18nBean.getText(errorKey));
            }
            else
            {
                errors.addError(fieldName, i18nBean.getText(errorKey));
            }
            return false;
        }
    }

    @Override
    public void validate(TaskParametersMap config, ErrorCollection errors)
    {
        // Plugin Artifact is always required
        if (!validateNotBlank(UiFields.PLUGIN_ARTIFACT, config.getArtifact(), "bcpd.config.form.artifact.error.required", errors))
        {
            errors.addError(UiFields.PLUGIN_ARTIFACT_NAME, i18nBean.getText("bcpd.config.form.artifact.error.required"));
        }
        else
        {
            // Ensure the value resolves to an actual artifact
            try
            {
                AvailableArtifact artifact = new AvailableArtifact(config.getArtifact());
                ArtifactDefinition artifactDefinition = artifactDefinitionManager.findArtifactDefinition(artifact.getArtifactId());

                if (artifactDefinition == null)
                {
                    errors.addError(UiFields.PLUGIN_ARTIFACT, i18nBean.getText("bcpd.config.form.artifact.error.invalid", new String[] {artifact.getName()}));
                }
            }
            catch (IllegalArgumentException e)
            {
                errors.addError(UiFields.PLUGIN_ARTIFACT, i18nBean.getText("bcpd.config.form.artifact.error.invalid", new String[] {config.getArtifact()}));
            }
        }

        // Base URL is always required
        if (validateNotBlank(UiFields.BASE_URL, config.getBaseUrl(), "bcpd.config.form.baseUrl.error.required", errors))
        {
            // Must be a well-formed URL
            validateIsUrl(UiFields.BASE_URL, config.getBaseUrl(), "bcpd.config.form.baseUrl.error.invalid", errors);
        }

        // Validate credentials...
        if (config.useAtlassianId())
        {
            // Atlassian ID Base URL must actually be a URL, or be empty (since a hard-coded default value exists).
            if (!StringUtils.isBlank(config.getAtlassianIdBaseUrl()))
            {
                validateIsUrl("", config.getAtlassianIdBaseUrl(), "bcpd.config.form.atlassianIdUrl.error.invalid", errors);
            }

            validateNotBlank("", config.getAtlassianIdUsername(), "bcpd.config.form.username.error.required", errors);
            validatePassword(config.useAtlassianIdPasswordVariable(), config.getAtlassianIdPasswordVariable(), config.getAtlassianIdPassword(), errors);

            // If using Atlassian ID WebSudo, App Name must be specified.
            if (config.useAtlassianIdWebSudo())
            {
                validateNotBlank("", config.getAtlassianIdAppName(), "bcpd.config.form.appname.error.required", errors);
            }
            // TODO: Support WebSudo just being completely disabled.
            else
            {
                // Otherwise, the password override must be specified.
                validatePassword(config.usePasswordVariable(), config.getPasswordVariable(), config.getPassword(), errors);
            }
        }
        else
        {
            validateNotBlank("", config.getUsername(), "bcpd.config.form.username.error.required", errors);
            validatePassword(config.usePasswordVariable(), config.getPasswordVariable(), config.getPassword(), errors);
        }
    }
}
