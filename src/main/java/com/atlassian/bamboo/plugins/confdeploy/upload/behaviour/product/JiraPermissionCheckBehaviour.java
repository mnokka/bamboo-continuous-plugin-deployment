package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.UrlUtils;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.fugue.Either;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;

/**
 * A {@link RequestBehaviour} that makes an explicit check to ensure that the configured user account for executing the
 * JIRA plugin installation is actually a system administrator of the target JIRA instance.
 *
 * This allows the deployment task to fail with a specific error message if the user does not have the necessary
 * permissions, rather than failing further down the pipeline with an ambiguous HTTP 401 error message.
 */
public class JiraPermissionCheckBehaviour implements RequestBehaviour
{
    /**
     * {@inheritDoc}
     */
    @Override
    public Either<HttpUriRequest, Failure> getRequest(TaskConfiguration configuration, Map<String, Object> requestContext)
    {
        final String url = UrlUtils.join(configuration.getRemoteBaseUrl(), "/rest/api/2/mypermissions");
        return Either.<HttpUriRequest, Failure>left(new HttpGet(url));
    }

    @Override
    public Result handleResponse(HttpResponse response) throws IOException
    {
        try
        {
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 200)
            {
                return Result.failure("Unexpected HTTP Status code " + statusCode + " was returned.");
            }

            // Examine the JSON Body for the SYSTEM_ADMIN permission.
            String entity = EntityUtils.toString(response.getEntity());
            try
            {
                JSONObject myPermissions = new JSONObject(entity);
                JSONObject permissions = myPermissions.getJSONObject("permissions");
                JSONObject SYSTEM_ADMIN = permissions.getJSONObject("SYSTEM_ADMIN");

                boolean hasPermission = SYSTEM_ADMIN.getBoolean("havePermission");
                if (!hasPermission)
                {
                    return Result.failure("The user configured for this task does not have system administration privileges on the target JIRA server.");
                }
            }
            catch (JSONException e)
            {
                return Result.failure("Failed to JSONify the permission check response.", e);
            }

            return Result.success("Permission check successful!");
        }
        finally
        {
            EntityUtils.consume(response.getEntity());
        }


    }
}
