package com.atlassian.bamboo.plugins.confdeploy.config;

import java.io.File;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask;
import com.atlassian.bamboo.plugins.confdeploy.Crypto;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.ConfigFields;
import com.atlassian.fugue.Either;

import org.apache.commons.lang.StringUtils;

/**
 * {@inheritDoc}
 */
public class DefaultTaskConfigurationFactory implements TaskConfigurationFactory
{
    private static final String DEFAULT_ATLASSIAN_ID_BASE_URL = "https://id.atlassian.com";

    private final ArtifactRetriever artifactRetriever;

    public DefaultTaskConfigurationFactory(final ArtifactRetriever artifactRetriever)
    {
        this.artifactRetriever = artifactRetriever;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Either<TaskConfiguration, Failure> getTaskConfiguration(final ConfigurationMap map)
    {

        // Extract primitive value parameters.
        final boolean branchEnabled = map.getAsBoolean(ConfigFields.ALLOW_BRANCH_BUILDS);
        final boolean allowCertificateErrors = map.getAsBoolean(ConfigFields.ALLOW_SSL_ERRORS);
        final boolean enableTrafficLogging = map.getAsBoolean(ConfigFields.ENABLE_TRAFFIC_LOGGING);

        // Do the box step
        Object maybePluginInstallationTimeout = map.get(ConfigFields.PLUGIN_INSTALLATION_TIMEOUT);
        final int pluginInstallationTimeout = maybePluginInstallationTimeout == null ? AutoDeployTask.DEFAULT_PLUGIN_INSTALLATION_TIMEOUT : Integer.valueOf(maybePluginInstallationTimeout.toString());

        final String remoteBaseUrl = map.get(ConfigFields.BASE_URL);
        final String username = map.get(ConfigFields.USERNAME);

        String password = "";

        final boolean useAtlassianId = map.getAsBoolean(ConfigFields.USE_ATLASSIAN_ID);
        boolean useAtlassianIdWebSudo = false;
        String atlassianIdBaseUrl = "";
        String atlassianIdUsername = "";
        String atlassianIdPassword = "";
        String atlassianIdAppName = "";
        if (useAtlassianId)
        {
            useAtlassianIdWebSudo = map.getAsBoolean(ConfigFields.USE_ATLASSIAN_ID_WEBSUDO);
            atlassianIdAppName = map.get(ConfigFields.ATLASSIAN_ID_APP_NAME);

            if (map.containsKey(ConfigFields.ATLASSIAN_ID_BASE_URL))
            {
                atlassianIdBaseUrl = map.get(ConfigFields.ATLASSIAN_ID_BASE_URL);
            }
            else
            {
                atlassianIdBaseUrl = DEFAULT_ATLASSIAN_ID_BASE_URL;
            }

            atlassianIdUsername = map.get(ConfigFields.ATLASSIAN_ID_USERNAME);
            atlassianIdPassword = getPassword(
                    map.get(ConfigFields.ATLASSIAN_ID_PASSWORD),
                    map.get(ConfigFields.ATLASSIAN_ID_PASSWORD_ENCRYPTION_KEY),
                    map.get(ConfigFields.ATLASSIAN_ID_PASSWORD_VARIABLE)
            );
        }

        if (!useAtlassianId || !useAtlassianIdWebSudo) {
            password = getPassword(
                    map.get(ConfigFields.PASSWORD),
                    map.get(ConfigFields.PASSWORD_ENCRYPTION_KEY),
                    map.get(ConfigFields.PASSWORD_VARIABLE)
            );
        }

        // Retrieve the referenced artifact definition
        Either<File, Failure> maybePluginJar = artifactRetriever.getFileFromArtifact(map.get(ConfigFields.PLUGIN_ARTIFACT));
        if (maybePluginJar.isRight())
            // Help I'm trapped in a functional coding paradigm.
            return Either.right(maybePluginJar.right().get());

        // Retrieve the configured product type, if defined.
        String productKey = map.get(ConfigFields.PRODUCT_TYPE);
        RemoteProductType productType = null;
        if (StringUtils.isNotBlank(productKey))
        {
            productType = RemoteProductType.fromKey(productKey);
        }

        return Either.<TaskConfiguration, Failure>left(new DefaultTaskConfiguration(
                productType,
                maybePluginJar.left().get(),
                remoteBaseUrl,
                username,
                password,
                useAtlassianId,
                useAtlassianIdWebSudo,
                atlassianIdUsername,
                atlassianIdPassword,
                atlassianIdBaseUrl,
                atlassianIdAppName,
                allowCertificateErrors,
                branchEnabled,
                enableTrafficLogging,
                pluginInstallationTimeout
        ));
    }

    private static String getPassword(String encryptedPassword, String decryptionKey, String passwordVariable)
    {
        return StringUtils.isBlank(passwordVariable) ? Crypto.decrypt(decryptionKey, encryptedPassword) : passwordVariable;
    }
}
