[@ww.label labelKey="bcpd.config.form.product" name="bcpd_config_product" /]
[@ww.label labelKey="bcpd.config.form.artifact" name="bcpd_config_artifact" /]
[@ww.label labelKey="bcpd.config.form.baseUrl" name="bcpd_config_baseUrl" /]
[@ww.label labelKey="bcpd.config.dialog.login.username" name="storedUser" /]

[@ww.label labelKey="bcpd.config.form.branching" name="bcpd_config_branching" /]
[@ww.label labelKey="bcpd.config.form.sslCheck" name="bcpd_config_ssl" /]
[@ww.label labelKey="bcpd.config.form.logging" name="bcpd_config_logging" /]
[@ww.label labelKey="bcpd.config.form.timeout" name="bcpd_config_timeout" /]
