package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.build.logger.NullBuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.PollingRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.RequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.HttpClientWrapper;
import com.atlassian.fugue.Either;
import com.atlassian.util.concurrent.Timeout;
import com.google.common.collect.Maps;
import org.apache.http.client.methods.HttpUriRequest;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static testsupport.MockTaskConfigurationFactory.*;

/**
 * Tests for {@link DefaultUploadClient}.
 */
public class TestDefaultUploadClient
{
    @Test
    public void testMockedHappyPath() throws Exception
    {
        HashMap<String,Object> parameters = Maps.newHashMap();
        parameters.put(PLUGIN_JAR, File.createTempFile("abc", "123"));

        final TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(parameters);
        HttpClientWrapper mockClientWrapper = mock(HttpClientWrapper.class);

        // Login
        RequestBehaviour login = mock(RequestBehaviour.class);
        HttpUriRequest loginRequest = mock(HttpUriRequest.class);
        when(login.getRequest(eq(mockConfig), any(Map.class))).thenReturn(Either.<HttpUriRequest, Failure>left(loginRequest));
        when(mockClientWrapper.execute(loginRequest, login)).thenReturn(Result.success("Login worked"));

        // WebSudo
        RequestBehaviour websudo = mock(RequestBehaviour.class);
        HttpUriRequest webSudoRequest = mock(HttpUriRequest.class);
        when(websudo.getRequest(eq(mockConfig), any(Map.class))).thenReturn(Either.<HttpUriRequest, Failure>left(webSudoRequest));
        when(mockClientWrapper.execute(webSudoRequest, websudo)).thenReturn(Result.success("WebSudo worked"));

        // Get UPM Token
        RequestBehaviour upmToken = mock(RequestBehaviour.class);
        HttpUriRequest upmTokenRequest = mock(HttpUriRequest.class);
        when(upmToken.getRequest(eq(mockConfig), any(Map.class))).thenReturn(Either.<HttpUriRequest, Failure>left(upmTokenRequest));
        when(mockClientWrapper.execute(upmTokenRequest, upmToken)).thenReturn(Result.success("UPM Token Request worked", "the-upm-token"));

        // Plugin Upload
        RequestBehaviour pluginUpload = mock(RequestBehaviour.class);
        HttpUriRequest pluginUploadRequest = mock(HttpUriRequest.class);
        // TODO: Assert UPM Token present in map.
        when(pluginUpload.getRequest(eq(mockConfig), any(Map.class))).thenReturn(Either.<HttpUriRequest, Failure>left(pluginUploadRequest));
        when(mockClientWrapper.execute(pluginUploadRequest, pluginUpload)).thenReturn(Result.success("Plugin upload worked"));

        // Success Check
        PollingRequestBehaviour successCheck = mock(PollingRequestBehaviour.class);
        HttpUriRequest successCheckRequest = mock(HttpUriRequest.class);
        // TODO: Assert JSONObject present in map.
        when(successCheck.getRequest(eq(mockConfig), any(Map.class))).thenReturn(Either.<HttpUriRequest, Failure>left(successCheckRequest));
        when(mockClientWrapper.poll(eq(successCheckRequest), eq(successCheck), any(Timeout.class))).thenReturn(Result.success("Upload check worked!"));

        final DefaultUploadClient client = new DefaultUploadClient(login, null, websudo, upmToken, pluginUpload, successCheck, null, null, new NullBuildLogger(), mockClientWrapper);

        Result deploy = client.deploy(mockConfig);
        assertTrue(deploy.isSuccess());
        assertEquals("Plugin upload completed successfully. Nice going, mate!", deploy.getMessage());

    }
}
