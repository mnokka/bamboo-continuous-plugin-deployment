package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.BambooLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.ConfluenceLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.FeCruLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.JiraLoginRequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;
import testsupport.assertions.FormEncodedRequestBodyAssertion;
import testsupport.assertions.HttpUriRequestAssertion;
import testsupport.assertions.HttpUriRequestAssertionHelper;
import testsupport.assertions.RequestUrlAssertion;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.PASSWORD;
import static testsupport.MockTaskConfigurationFactory.USERNAME;

/**
 * Test harness for {@link ProductLoginRequestBehaviour} and its known sub-classes.
 */
public class TestProductLoginRequestBehaviour
{
    private static final String TEST_BASE_URL = "https://notarealsystem.mycompany.com";
    private static final String TEST_USERNAME = "santana";
    private static final String TEST_PASSWORD = "blackmagicwoman";

    @Test
    public void testConfluence()
    {
        RequestBehaviour clrb = new ConfluenceLoginRequestBehaviour(TEST_BASE_URL);

        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
            USERNAME, TEST_USERNAME,
            PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = clrb.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequestAssertionHelper.assertRequest(maybeRequest.left().get(), ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/dologin.action"),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("os_username", TEST_USERNAME),
                        new BasicNameValuePair("os_password", TEST_PASSWORD)
                ))
        ));
    }

    @Test
    public void testJira()
    {
        RequestBehaviour jlrb = new JiraLoginRequestBehaviour(TEST_BASE_URL);

        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, TEST_USERNAME,
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = jlrb.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequestAssertionHelper.assertRequest(maybeRequest.left().get(), ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/login.jsp"),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("os_username", TEST_USERNAME),
                        new BasicNameValuePair("os_password", TEST_PASSWORD)
                ))
        ));
    }

    @Test
    public void testBamboo()
    {
        RequestBehaviour jlrb = new BambooLoginRequestBehaviour(TEST_BASE_URL);

        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, TEST_USERNAME,
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = jlrb.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequestAssertionHelper.assertRequest(maybeRequest.left().get(), ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/userlogin.action"),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("os_username", TEST_USERNAME),
                        new BasicNameValuePair("os_password", TEST_PASSWORD)
                ))
        ));
    }

    @Test
    public void testFeCru()
    {
        RequestBehaviour jlrb = new FeCruLoginRequestBehaviour(TEST_BASE_URL);

        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, TEST_USERNAME,
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = jlrb.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequestAssertionHelper.assertRequest(maybeRequest.left().get(), ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/login"),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("username", TEST_USERNAME),
                        new BasicNameValuePair("password", TEST_PASSWORD)
                ))
        ));
    }

    @Test
    public void testResponseSuccess() throws IOException
    {
        RequestBehaviour requestBehaviour = new JiraLoginRequestBehaviour(TEST_BASE_URL);


        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(200);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertTrue(result.isSuccess());
    }

    @Test
    public void testResponseFailure() throws IOException
    {
        RequestBehaviour requestBehaviour = new JiraLoginRequestBehaviour(TEST_BASE_URL);

        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(500);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertFalse(result.isSuccess());
    }
}
