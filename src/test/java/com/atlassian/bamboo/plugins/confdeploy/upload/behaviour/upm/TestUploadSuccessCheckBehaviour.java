package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.PollingRequestBehaviour;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link UploadSuccessCheckBehaviour}
 */
public class TestUploadSuccessCheckBehaviour
{
    private static final String TEST_BASE_URL = "https://pug.jira.com/wiki";
    private static final String TEST_RESPONSE_ENTITY_NOPINGAFTER =
            "{" +
                    "\"links\": { " +
                        "\"self\": \"wiki/rest/plugins/1.0/pending/cefaed2e-9ff1-432c-ae0d-81a6f3815e16\"" +
                    "}" +
            "}";

    private static final String TEST_RESPONSE_ENTITY_PINGAFTER =
            "{" +
                    "\"pingAfter\": 20, " +
                    "\"links\": {" +
                        "\"self\": \"wiki/rest/plugins/1.0/pending/cefaed2e-9ff1-432c-ae0d-81a6f3815e16\"" +
                    "}" +
            "}";

    private PollingRequestBehaviour uploadSuccessCheckBehaviour;
    private TaskConfiguration mockConfig;

    @Before
    public void setUp()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(Maps.<String, Object>newHashMap());
        when(mockConfig.getRemoteBaseUrl()).thenReturn(TEST_BASE_URL);
        uploadSuccessCheckBehaviour = new UploadSuccessCheckBehaviour(mockConfig);
    }

    @Test
    public void testGetRequestMissingContext()
    {
        Either<HttpUriRequest,Failure> request = uploadSuccessCheckBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(request.isRight());
        assertEquals("Could not check plugin install status - the JSON Response entity is missing (probably a bug!)", request.right().get().getMessage());
    }

    @Test
    public void testGetRequest() throws Exception
    {
        JSONObject responseEntity = new JSONObject(TEST_RESPONSE_ENTITY_NOPINGAFTER);

        Either<HttpUriRequest,Failure> request = uploadSuccessCheckBehaviour.getRequest(mockConfig, ImmutableMap.<String, Object>of("responseEntity", responseEntity));
        assertTrue(request.isLeft());

        String actualUrl = request.left().get().getURI().toASCIIString();
        String expectedUrl = "https://pug.jira.com/wiki/rest/plugins/1.0/pending/cefaed2e-9ff1-432c-ae0d-81a6f3815e16";

        assertTrue(actualUrl.startsWith(expectedUrl));
    }

    @Test
    public void testHandleResponse_Success() throws Exception
    {
        HttpResponse mockResponse = mock(HttpResponse.class);
        StatusLine mockStatusLine = mock(StatusLine.class);

        when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
        when(mockStatusLine.getStatusCode()).thenReturn(200);
        when(mockResponse.getEntity()).thenReturn(new StringEntity(TEST_RESPONSE_ENTITY_NOPINGAFTER));

        Either<HttpUriRequest,Result> result = uploadSuccessCheckBehaviour.apply(mockResponse);
        assertTrue(result.isRight());
        assertTrue(result.right().get().isSuccess());
        assertEquals("Plugin installed successfully", result.right().get().getMessage());
    }

    @Test
    public void testHandleResponse_KeepPolling() throws Exception
    {
        HttpResponse mockResponse = mock(HttpResponse.class);
        StatusLine mockStatusLine = mock(StatusLine.class);

        when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
        when(mockStatusLine.getStatusCode()).thenReturn(200);
        when(mockResponse.getEntity()).thenReturn(new StringEntity(TEST_RESPONSE_ENTITY_PINGAFTER));

        Either<HttpUriRequest, Result> result = uploadSuccessCheckBehaviour.apply(mockResponse);
        assertTrue(result.isLeft());

        String actualUrl = result.left().get().getURI().toASCIIString();
        String expectedUrl = "https://pug.jira.com/wiki/rest/plugins/1.0/pending/cefaed2e-9ff1-432c-ae0d-81a6f3815e16";

        assertTrue(actualUrl.startsWith(expectedUrl));
    }

    @Test
    public void testHandleResponse_Failure() throws Exception
    {
        HttpResponse mockResponse = mock(HttpResponse.class);
        StatusLine mockStatusLine = mock(StatusLine.class);

        when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
        when(mockStatusLine.getStatusCode()).thenReturn(401);

        Either<HttpUriRequest, Result> result = uploadSuccessCheckBehaviour.apply(mockResponse);
        assertTrue(result.isRight());
        assertFalse(result.right().get().isSuccess());
        assertEquals("Unexpected response code when polling upload: 401", result.right().get().getMessage());
    }
}
