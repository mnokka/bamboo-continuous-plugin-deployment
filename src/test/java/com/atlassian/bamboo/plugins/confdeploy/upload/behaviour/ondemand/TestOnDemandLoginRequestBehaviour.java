package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ondemand;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;
import testsupport.assertions.*;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.PASSWORD;
import static testsupport.MockTaskConfigurationFactory.USERNAME;

/**
 * Tests for {@link OnDemandLoginRequestBehaviour}
 */
public class TestOnDemandLoginRequestBehaviour
{
    private static final String TEST_BASE_URL = "https://ecosystem.atlassian.net";
    private static final String TEST_USERNAME = "bobdylan";
    private static final String TEST_PASSWORD = "hurricane";

    private OnDemandLoginRequestBehaviour requestBehaviour;
    private TaskConfiguration mockConfig;

    @Before
    public void setUp()
    {
        requestBehaviour = new OnDemandLoginRequestBehaviour(TEST_BASE_URL);
    }

    /**
     * Ensures that request creation fails if the username is not specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUsernameIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, "",
                PASSWORD, TEST_PASSWORD
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures that request creation fails if the password is not specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testPasswordIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, TEST_USERNAME,
                PASSWORD, ""
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Tests successful request creation.
     */
    @Test
    public void testGetRequest() throws IOException
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, TEST_USERNAME,
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());
        HttpUriRequest request = maybeRequest.left().get();

        HttpUriRequestAssertionHelper.assertRequest(request, ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/login"),
                new MethodTypeAssertion(MethodTypeAssertion.Method.POST),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("username", TEST_USERNAME),
                        new BasicNameValuePair("password", TEST_PASSWORD)
                ))
        ));
    }

    /**
     * Ensures the login URL is always calculated relative to the root of the OnDemand hostname, not the specific application
     * within OnDemand.
     */
    @Test
    public void testRequestUrlConstructedCorrectly()
    {
        OnDemandLoginRequestBehaviour requestBehaviour = new OnDemandLoginRequestBehaviour("https://ecosystem.atlassian.net/wiki");

        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                USERNAME, TEST_USERNAME,
                PASSWORD, TEST_PASSWORD
        ));

        Either<HttpUriRequest,Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());

        HttpUriRequest request = maybeRequest.left().get();
        // IMPORTANT: The /wiki context path gets removed from the login URL. This is deliberate.
        assertEquals("https://ecosystem.atlassian.net/login", request.getURI().toASCIIString());
    }

    /**
     * Ensures that the response handler correctly identifies a success condition
     */
    @Test
    public void testResponseSuccess() throws IOException
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(200);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertTrue(result.isSuccess());
    }

    /**
     * Ensures that the response handler correctly identifies a failure condition
     */
    @Test
    public void testResponseFailure() throws IOException
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(401);
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertFalse(result.isSuccess());
    }
}
