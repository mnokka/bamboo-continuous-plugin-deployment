package com.atlassian.bamboo.plugins.confdeploy.pageobjects;

import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.UiFields;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import javax.inject.Inject;
import java.util.Map;

public class DeployPluginTaskComponent implements TaskComponent
{
    @Inject
    protected AtlassianWebDriver driver;

    @Inject
    protected PageBinder binder;

    @FindBy(id = UiFields.PLUGIN_ARTIFACT)
    private WebElement pluginArtifact;

    @FindBy(id = UiFields.BASE_URL)
    private WebElement baseUrl;

    @FindBy(id = UiFields.EDIT_CREDENTIALS)
    private WebElement editCredentialsButton;

    @FindBy(id = UiFields.ALLOW_BRANCH_BUILDS)
    private WebElement runOnBranchBuilds;

    @FindBy(id = UiFields.ALLOW_SSL_ERRORS)
    private WebElement disableSSLCertificateCheck;

    @FindBy(id = UiFields.ENABLE_TRAFFIC_LOGGING)
    private WebElement dumpTrafficToLog;

    @FindBy(id = UiFields.PLUGIN_INSTALLATION_TIMEOUT)
    private WebElement pluginInstallationTimeout;

    // TODO: Do we need to bind the hidden input elements? Probably.

    public DeployPluginTaskComponent()
    {
        // no-op
        ;
    }

    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        // TODO: Allow the artifact to be selected.
        Select selector = new Select(pluginArtifact);
        selector.selectByIndex(0); // select the first available artifact for now. TODO: improve this.

        // Set Base URL
        if (config.containsKey(UiFields.BASE_URL))
        {
            baseUrl.sendKeys(config.get(UiFields.BASE_URL));
        }

        // Enter deployment credentials into the dialog
        editCredentialsButton.click();
        EditCredentialsDialog dialog = binder.bind(EditCredentialsDialog.class);
        dialog.updateCredentials(config);

        // TODO: Handle other fields
    }
}
