package com.atlassian.bamboo.plugins.confdeploy.config;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link DefaultTaskConfiguration}.
 */
public class TestTaskConfiguration
{
    private static final String TEST_BASE_URL = "https://remoteapps.jira.com";
    private static final String TEST_USERNAME = "eric-clapton";
    private static final String TEST_PASSWORD = "cream";
    private static final boolean TEST_USE_ATLASSIAN_ID = true;
    private static final boolean TEST_USE_ATLASSIAN_ID_WEBSUDO = true;
    private static final String TEST_ATLASSIAN_ID_USERNAME = "royorbison";
    private static final String TEST_ATLASSIAN_ID_PASSWORD = "prettywoman";
    private static final String TEST_ATLASSIAN_ID_BASE_URL = "https://id.stg.iam.atlassian.com";
    private static final String TEST_ATLASSIAN_ID_APPNAME = "quac";
    private static final boolean TEST_ALLOW_CERTIFICATE_ERRORS = true;
    private static final boolean TEST_ALLOW_BRANCH_BUILDS = true;
    private static final boolean TEST_ENABLE_TRAFFIC_LOGGING = true;
    private static final int TEST_PLUGIN_INSTALLATION_TIMEOUT = 42;

    /**
     * Basic constructor/getter test.
     */
    @Test
    public void testDefaultTaskConfigurationConstruction() throws IOException
    {
        final File testPluginJar = File.createTempFile("abc", "123");

        TaskConfiguration config = new DefaultTaskConfiguration(
                null,
                testPluginJar,
                TEST_BASE_URL,
                TEST_USERNAME,
                TEST_PASSWORD,
                TEST_USE_ATLASSIAN_ID,
                TEST_USE_ATLASSIAN_ID_WEBSUDO,
                TEST_ATLASSIAN_ID_USERNAME,
                TEST_ATLASSIAN_ID_PASSWORD,
                TEST_ATLASSIAN_ID_BASE_URL,
                TEST_ATLASSIAN_ID_APPNAME,
                TEST_ALLOW_CERTIFICATE_ERRORS,
                TEST_ALLOW_BRANCH_BUILDS,
                TEST_ENABLE_TRAFFIC_LOGGING,
                TEST_PLUGIN_INSTALLATION_TIMEOUT
        );

        assertEquals(testPluginJar, config.getPluginJar());
        assertEquals(TEST_BASE_URL, config.getRemoteBaseUrl());
        assertEquals(TEST_USERNAME, config.getUsername());
        assertEquals(TEST_PASSWORD, config.getPassword());
        assertEquals(TEST_USE_ATLASSIAN_ID, config.useAtlassianId());
        assertEquals(TEST_USE_ATLASSIAN_ID_WEBSUDO, config.useAtlassianIdWebSudo());
        assertEquals(TEST_ATLASSIAN_ID_USERNAME, config.getAtlassianIdUsername());
        assertEquals(TEST_ATLASSIAN_ID_PASSWORD, config.getAtlassianIdPassword());
        assertEquals(TEST_ATLASSIAN_ID_BASE_URL, config.getAtlassianIdBaseUrl());
        assertEquals(TEST_ATLASSIAN_ID_APPNAME, config.getAtlassianIdAppName());
        assertEquals(TEST_ALLOW_CERTIFICATE_ERRORS, config.allowSSLCertificateErrors());
        assertEquals(TEST_ALLOW_BRANCH_BUILDS, config.allowBranchBuildExecution());
        assertEquals(TEST_ENABLE_TRAFFIC_LOGGING, config.getEnableTrafficLogging());
        assertEquals(TEST_PLUGIN_INSTALLATION_TIMEOUT, config.getPluginInstallationTimeout());
    }

}
