package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.DefaultHttpClientWrapper;
import com.atlassian.bamboo.plugins.confdeploy.upload.http.HttpClientWrapper;
import com.atlassian.fugue.Either;
import com.atlassian.util.concurrent.Timeout;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.base.Function;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

/**
 * Tests for {@link com.atlassian.bamboo.plugins.confdeploy.upload.http.DefaultHttpClientWrapper}.
 *
 * Not a true unit test, since it's also exercising some behaviour in {@link com.atlassian.bamboo.plugins.confdeploy.upload.http.BuildLoggingRequestInterceptor}.
 */
public class TestDefaultHttpClientWrapper
{
    private static final String TEST_RESPONSE_BODY = "{\"working\": \"yes\"}";

    private HttpClientWrapper wrapper;
    private TaskConfiguration taskConfiguration;
    BuildLogger mockLogger;

    @Before
    public void setUp()
    {
        mockLogger = Mockito.mock(BuildLogger.class);
        taskConfiguration = Mockito.mock(TaskConfiguration.class);
        wrapper = new DefaultHttpClientWrapper(mockLogger, taskConfiguration);
    }

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089, 8444); // No-args constructor defaults to port 8080

    private void stubSimpleHttpResponse()
    {
        stubFor(get(urlEqualTo("/test"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(TEST_RESPONSE_BODY)));
    }

    private void stubHttpPOSTAndResponse()
    {
        stubFor(post(urlEqualTo("/submit"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(TEST_RESPONSE_BODY)));
    }

    /**
     * Tests that the HTTP Client wrapper can connect to an HTTPS service using an untrusted server certificate,
     * provided that the "allowCertificateErrors" option is enabled.
     *
     */
    @Test
    public void testAllowCertificateErrors() throws Exception
    {
        Mockito.when(taskConfiguration.allowSSLCertificateErrors()).thenReturn(true);
        wrapper = new DefaultHttpClientWrapper(mockLogger, taskConfiguration);

        stubSimpleHttpResponse();

        Result result = wrapper.execute(new HttpGet("https://localhost:8444/test"), new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws ClientProtocolException, IOException
            {
                // It worked!
                return Result.success("Yeah!!");
            }
        });
        assertTrue(result.isSuccess());
        assertTrue(result.getMessage().equals("Yeah!!"));
    }

    /**
     * Tests that the HTTP Client wrapper fails gracefully when attempting to connect to an HTTPS service using an
     * untrusted server certificate.
     */
    @Test
    public void testFailWhenServerCertificateInvalid() throws Exception
    {
        stubSimpleHttpResponse();

        Result result = wrapper.execute(new HttpGet("https://localhost:8444/test"), new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws ClientProtocolException, IOException
            {
                System.out.println("SSL connection worked even though the server certificate should not be trusted.");
                fail("sadtrombone.mp3");
                throw new RuntimeException("Do not execute me");
            }
        });
        assertFalse(result.isSuccess());
        assertTrue(result.getMessage().contains("sun.security.provider.certpath.SunCertPathBuilderException"));
    }

    /**
     * Tests executing a single HTTP request and handling the response.
     */
    @Test
    public void testHttpRequestExecution() throws Exception
    {
       stubSimpleHttpResponse();

        Result result = wrapper.execute(new HttpGet("http://localhost:8089/test"), new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws IOException
            {
                try
                {
                    JSONObject j = new JSONObject(EntityUtils.toString(response.getEntity()));

                    assertEquals("yes", j.getString("working"));
                } catch (JSONException e)
                {
                    fail(e.toString());
                }
                return Result.success("It worked");
            }
        });
        assertTrue(result.isSuccess());
        assertEquals("It worked", result.getMessage());

        // Ensure debug logging is disabled
        Mockito.verify(mockLogger, Mockito.never()).addBuildLogEntry("**** Outgoing Request Debug Log ****");
        Mockito.verify(mockLogger, Mockito.never()).addBuildLogEntry("**** Incoming Response Debug Log ****");
    }

    /**
     * Tests that request and response details are dumped to the build log when the "enable traffic dump" option is
     * enabled.
     */
    @Test
    public void testHttpRequestExecutionWithTrafficDump()
    {
        mockLogger = Mockito.mock(BuildLogger.class);
        Mockito.when(taskConfiguration.getEnableTrafficLogging()).thenReturn(true);
        wrapper = new DefaultHttpClientWrapper(mockLogger, taskConfiguration);

        stubSimpleHttpResponse();

        wrapper.execute(new HttpGet("http://localhost:8089/test"), new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws IOException
            {
                EntityUtils.consume(response.getEntity());
                return Result.success("Woot");
            }
        });

        // Verify that the build logger received the traffic dump.
        // request
        Mockito.verify(mockLogger).addBuildLogEntry("**** Outgoing Request Debug Log (http://localhost:8089) ****");
        Mockito.verify(mockLogger).addBuildLogEntry("> GET /test HTTP/1.1");

        // response
        Mockito.verify(mockLogger).addBuildLogEntry("**** Incoming Response Debug Log ****");
        Mockito.verify(mockLogger).addBuildLogEntry("< HTTP/1.1 200 OK");
    }

    /**
     * Tests that request and response details are dumped to the build log when the "enable traffic dump" option is
     * enabled and the request has a body.
     */
    @Test
    public void testBodiedHttpRequestExecutionWithTrafficDump() throws Exception
    {
        mockLogger = Mockito.mock(BuildLogger.class);
        Mockito.when(taskConfiguration.getEnableTrafficLogging()).thenReturn(true);
        wrapper = new DefaultHttpClientWrapper(mockLogger, taskConfiguration);

        stubHttpPOSTAndResponse();

        HttpPost request = new HttpPost("http://localhost:8089/submit");
        request.setEntity(new StringEntity("Hello, World"));

        wrapper.execute(request, new ResponseHandler<Result>()
        {
            @Override
            public Result handleResponse(HttpResponse response) throws IOException
            {
                EntityUtils.consume(response.getEntity());
                return Result.success("Woot");
            }
        });

        // Verify that the build logger received the traffic dump.
        // request
        Mockito.verify(mockLogger).addBuildLogEntry("**** Outgoing Request Debug Log (http://localhost:8089) ****");
        Mockito.verify(mockLogger).addBuildLogEntry("> POST /submit HTTP/1.1");
        Mockito.verify(mockLogger).addBuildLogEntry("> Hello, World");

        // response
        Mockito.verify(mockLogger).addBuildLogEntry("**** Incoming Response Debug Log ****");
        Mockito.verify(mockLogger).addBuildLogEntry("< HTTP/1.1 200 OK");
        Mockito.verify(mockLogger).addBuildLogEntry("< " + TEST_RESPONSE_BODY);
    }


    /**
     * Tests that the http client polling logic succeeds in a 'happy path' scenario.
     */
    @Test
    public void testHttpRequestPollingUntilSuccess() throws Exception
    {
        stubSimpleHttpResponse();

        Result result = wrapper.poll(new HttpGet("http://localhost:8089/test"), new Function<HttpResponse, Either<HttpUriRequest, Result>>()
        {
            private int count = 0;

            @Override
            public Either<HttpUriRequest, Result> apply(HttpResponse response)
            {
                try
                {
                    // Assert that the request was successful.
                    assertEquals(200, response.getStatusLine().getStatusCode());
                    assertEquals(TEST_RESPONSE_BODY, EntityUtils.toString(response.getEntity()));
                    EntityUtils.consume(response.getEntity());

                    // just poll 5 times and then succeed.
                    count++;

                    if (count == 5)
                    {
                        return Either.<HttpUriRequest, Result>right(Result.success("Polling succeeded!", count));
                    }
                }
                catch (IOException e)
                {
                    fail(e.toString());
                }

                return Either.<HttpUriRequest, Result>left(new HttpGet("http://localhost:8089/test"));
            }
        }, Timeout.getMillisTimeout(60, TimeUnit.SECONDS));

        // External resource should have been hit 5 times.
        verify(5, getRequestedFor(urlEqualTo("/test")));

        assertTrue(result.isSuccess());
        assertNotNull(result.getContext());
        assertTrue(result.getContext() instanceof Integer);
        int resultContext = (Integer)result.getContext();
        assertEquals(5, resultContext);
    }

    @Test
    public void testHttpRequestPollingUntilFailures() throws Exception
    {
        stubSimpleHttpResponse();

        final Result expectedResult = Result.failure("Nope.");

        Result actualResult = wrapper.poll(new HttpGet("http://localhost:8089/test"), new Function<HttpResponse, Either<HttpUriRequest, Result>>()
        {
            @Override
            public Either<HttpUriRequest, Result> apply(HttpResponse response)
            {
                try
                {
                    EntityUtils.consume(response.getEntity());
                }
                catch (IOException e)
                {
                    fail(e.toString());
                }

                return Either.right(expectedResult);
            }
        }, Timeout.getMillisTimeout(60, TimeUnit.SECONDS));

        // External resource should have been exactly once.
        verify(1, getRequestedFor(urlEqualTo("/test")));

        assertFalse(actualResult.isSuccess());
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testHttpRequestPollingUntilTimeout() throws Exception
    {
        stubSimpleHttpResponse();

        Result result = wrapper.poll(new HttpGet("http://localhost:8089/test"), new Function<HttpResponse, Either<HttpUriRequest, Result>>()
        {
            @Override
            public Either<HttpUriRequest, Result> apply(HttpResponse response)
            {
                try
                {
                    EntityUtils.consume(response.getEntity());
                }
                catch (IOException e)
                {
                    fail(e.toString());
                }
                return Either.left((HttpUriRequest)new HttpGet("http://localhost:8089/test"));
            }
        }, Timeout.getMillisTimeout(2, TimeUnit.SECONDS));

        assertFalse(result.isSuccess());
        assertEquals(result.getMessage(), "Polling request did not complete within the timeout.");
    }
}
