package com.atlassian.bamboo.plugins.confdeploy.upload;

import com.atlassian.bamboo.build.logger.NullBuildLogger;
import com.atlassian.bamboo.plugins.confdeploy.config.RemoteProductType;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso.AtlassianIdLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso.AtlassianIdWebSudoRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.ondemand.OnDemandLoginRequestBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.product.*;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm.UpmSelfUpdateBehaviour;
import com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.upm.UpmSelfUpdateCheckBehaviour;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;

import java.io.File;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static testsupport.MockTaskConfigurationFactory.*;

/**
 * Tests for {@link DefaultUploadClientFactory}
 */
public class TestDefaultUploadClientFactory
{
    private UploadClientFactory uploadClientFactory;

    @Before
    public void setUp()
    {
        uploadClientFactory = new DefaultUploadClientFactory();
    }

    @Test
    public void testSetupForVanillaJira() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
            BASE_URL, "https://jira.mycompany.com",
            USERNAME, "username",
            PASSWORD, "password",
            PLUGIN_JAR, File.createTempFile("abc", "123")
        ));
        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.JIRA, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof JiraLoginRequestBehaviour);
        assertTrue(uploadClient.getWebSudoStrategy() instanceof JiraWebSudoRequestBehaviour);
    }

    @Test
    public void testSetupForOnDemandJira() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                BASE_URL, "https://mycompany.jira.com", // ondemand detected by URL pattern
                USERNAME, "username",
                PASSWORD, "password",
                PLUGIN_JAR, File.createTempFile("abc", "123")
        ));

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.JIRA, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof OnDemandLoginRequestBehaviour);
        assertTrue(uploadClient.getWebSudoStrategy() instanceof JiraWebSudoRequestBehaviour);
    }

    @Test
    public void testSetupForAtlassianIdJira() throws Exception
    {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put(BASE_URL, "https://jira.atlassian.com");
        builder.put(USERNAME, "username");
        builder.put(PASSWORD, "password");
        builder.put(USE_ATLASSIAN_ID, true);
        builder.put(ATLASSIAN_ID_BASE_URL, "https://id.atlassian.com");
        builder.put(ATLASSIAN_ID_USERNAME, "anotherusername");
        builder.put(ATLASSIAN_ID_PASSWORD, "anotherpassword");
        builder.put(PLUGIN_JAR, File.createTempFile("abc", "123"));
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(builder.build());

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.JIRA, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof AtlassianIdLoginRequestBehaviour);
        assertTrue(uploadClient.getWebSudoStrategy() instanceof JiraWebSudoRequestBehaviour);
    }

    @Test
    public void testSetupForAtlassianIdJiraWithWebSudo() throws Exception
    {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();
        builder.put(BASE_URL, "https://jira.atlassian.com");
        builder.put(USERNAME, "username");
        builder.put(PASSWORD, "password");
        builder.put(USE_ATLASSIAN_ID, true);
        builder.put(ATLASSIAN_ID_BASE_URL, "https://id.atlassian.com");
        builder.put(ATLASSIAN_ID_USERNAME, "anotherusername");
        builder.put(ATLASSIAN_ID_PASSWORD, "anotherpassword");
        builder.put(USE_ATLASSIAN_ID_WEBSUDO, true);
        builder.put(ATLASSIAN_ID_APP_NAME, "qanda");
        builder.put(PLUGIN_JAR, File.createTempFile("abc", "123"));
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(builder.build());

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.JIRA, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof AtlassianIdLoginRequestBehaviour);
        assertTrue(uploadClient.getWebSudoStrategy() instanceof AtlassianIdWebSudoRequestBehaviour);
    }

    @Test
    public void testSetupForVanillaConfluence() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                BASE_URL, "https://wiki.mycompany.com/",
                USERNAME, "username",
                PASSWORD, "password",
                PLUGIN_JAR, File.createTempFile("abc", "123")
        ));

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.CONFLUENCE, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof ConfluenceLoginRequestBehaviour);
        assertTrue(uploadClient.getWebSudoStrategy() instanceof ConfluenceWebSudoRequestBehaviour);
    }

    @Test
    public void testSetupForVanillaBamboo() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                BASE_URL, "https://extranet-bamboo.internal.atlassian.com/",
                USERNAME, "username",
                PASSWORD, "password",
                PLUGIN_JAR, File.createTempFile("abc", "123")
        ));

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.BAMBOO, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof BambooLoginRequestBehaviour);
        assertNull(uploadClient.getWebSudoStrategy()); // bamboo doesn't support websudo
    }

    @Test
    public void testSetupForVanillaFeCru() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                BASE_URL, "https://fisheye.atlassian.com/",
                USERNAME, "username",
                PASSWORD, "password",
                PLUGIN_JAR, File.createTempFile("abc", "123")
        ));

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.FECRU, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getLoginStrategy() instanceof FeCruLoginRequestBehaviour);
        assertNull(uploadClient.getWebSudoStrategy()); // bamboo doesn't support websudo
    }

    @Test
    public void testSetupForUpm() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                BASE_URL, "https://myjira.mycompany.com/",
                USERNAME, "username",
                PASSWORD, "password",
                PLUGIN_JAR, File.createTempFile("atlassian-universal-plugin-manager-plugin", "123")
        ));

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.FECRU, mockConfig, new NullBuildLogger());

        assertTrue(uploadClient.getUpmSelfUpdateStrategy() instanceof UpmSelfUpdateBehaviour);
        assertTrue(uploadClient.getUpmSelfUpdateCheckStrategy() instanceof UpmSelfUpdateCheckBehaviour);
    }

    @Test
    public void testSetupNotUpm() throws Exception
    {
        TaskConfiguration mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                BASE_URL, "https://myjira.mycompany.com/",
                USERNAME, "username",
                PASSWORD, "password",
                PLUGIN_JAR, File.createTempFile("abc", "123")
        ));

        DefaultUploadClient uploadClient = uploadClientFactory.getUploadClient(RemoteProductType.FECRU, mockConfig, new NullBuildLogger());

        assertNull(uploadClient.getUpmSelfUpdateStrategy());
        assertNull(uploadClient.getUpmSelfUpdateCheckStrategy());
    }
}
