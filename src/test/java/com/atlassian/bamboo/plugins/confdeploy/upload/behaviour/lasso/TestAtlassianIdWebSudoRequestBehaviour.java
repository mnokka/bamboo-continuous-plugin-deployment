package com.atlassian.bamboo.plugins.confdeploy.upload.behaviour.lasso;

import com.atlassian.bamboo.plugins.confdeploy.Failure;
import com.atlassian.bamboo.plugins.confdeploy.Result;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;
import com.atlassian.fugue.Either;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.Test;
import testsupport.MockTaskConfigurationFactory;
import testsupport.assertions.FormEncodedRequestBodyAssertion;
import testsupport.assertions.HttpUriRequestAssertion;
import testsupport.assertions.HttpUriRequestAssertionHelper;
import testsupport.assertions.RequestUrlAssertion;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static testsupport.MockTaskConfigurationFactory.*;

/**
 * Tests {@link AtlassianIdWebSudoRequestBehaviour}
 */
public class TestAtlassianIdWebSudoRequestBehaviour
{
    private static final String TEST_BASE_URL = "https://id.atlassian.com";
    private static final String TEST_USERNAME = "billyjean";
    private static final String TEST_PASSWORD = "notmylover";
    private static final String TEST_APP_NAME = "neverland";

    private AtlassianIdWebSudoRequestBehaviour requestBehaviour;
    private TaskConfiguration mockConfig;

    @Before
    public void setUp()
    {
        requestBehaviour = new AtlassianIdWebSudoRequestBehaviour();
    }

    /**
     * Ensures that the request creation fails if the Atlassian ID Base URL is not specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAtlassianIdBaseUrlIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, "",
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD,
                ATLASSIAN_ID_APP_NAME, TEST_APP_NAME
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures that the request creation fails if the Atlassian ID App Name is not specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAtlassianIdAppNameIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD,
                ATLASSIAN_ID_APP_NAME, ""
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures that the request creation fails if the Atlassian ID Username is not specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAtlassianIdUsernameIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, "",
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD,
                ATLASSIAN_ID_APP_NAME, TEST_APP_NAME
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Ensures that the request creation fails if the Atlassian ID Password is not specified.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAtlassianIdPasswordIsRequired()
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, "",
                ATLASSIAN_ID_APP_NAME, TEST_APP_NAME
        ));

        requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap()); // should throw
    }

    /**
     * Tests that the outgoing request is created successfully when all required parameters are supplied.
     */
    @Test
    public void testRequestCreated() throws IOException
    {
        mockConfig = MockTaskConfigurationFactory.getMockTaskConfiguration(ImmutableMap.<String, Object>of(
                ATLASSIAN_ID_BASE_URL, TEST_BASE_URL,
                ATLASSIAN_ID_USERNAME, TEST_USERNAME,
                ATLASSIAN_ID_PASSWORD, TEST_PASSWORD,
                ATLASSIAN_ID_APP_NAME, TEST_APP_NAME
        ));

        Either<HttpUriRequest,Failure> maybeRequest = requestBehaviour.getRequest(mockConfig, Maps.<String, Object>newHashMap());
        assertTrue(maybeRequest.isLeft());
        HttpUriRequest request = maybeRequest.left().get();

        HttpUriRequestAssertionHelper.assertRequest(request, ImmutableList.<HttpUriRequestAssertion>of(
                new RequestUrlAssertion(TEST_BASE_URL + "/id/websudo.action?application=" + TEST_APP_NAME),
                new FormEncodedRequestBodyAssertion(ImmutableList.<NameValuePair>of(
                        new BasicNameValuePair("username", TEST_USERNAME),
                        new BasicNameValuePair("password", TEST_PASSWORD),
                        new BasicNameValuePair("websudo", "false")
                ))
        ));
    }

    /**
     * Ensures that the response handler correctly identifies a success condition
     */
    @Test
    public void testResponseSuccess() throws IOException
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(302);  // 302 is the only expected success response for Atlassian ID WebSudo.
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertTrue(result.isSuccess());
    }

    /**
     * Ensures that the response handler correctly identifies a failure condition
     */
    @Test
    public void testResponseFailure() throws IOException
    {
        StatusLine statusLine = mock(StatusLine.class);
        when(statusLine.getStatusCode()).thenReturn(200);  // Trust me, 200 is a failure here.
        HttpResponse response = mock(HttpResponse.class);
        when(response.getStatusLine()).thenReturn(statusLine);

        Result result = requestBehaviour.handleResponse(response);
        assertFalse(result.isSuccess());
    }
}
