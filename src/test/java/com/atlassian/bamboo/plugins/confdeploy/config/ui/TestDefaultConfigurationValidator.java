package com.atlassian.bamboo.plugins.confdeploy.config.ui;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.bamboo.utils.i18n.I18nBean;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test harness for {@link DefaultConfigurationValidator}
 */
public class TestDefaultConfigurationValidator
{
    private ConfigurationValidator configurationValidator;

    @Mock
    private I18nBean i18nBean;
    @Mock
    private ArtifactDefinitionManager artifactDefinitionManager;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        // TextProvider just echoes back the error key it was given
        when(i18nBean.getText(anyString())).thenAnswer(new Answer<String>()
        {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[0].toString();
            }
        });
        when(i18nBean.getText(anyString(), any(String[].class))).thenAnswer(new Answer<String>()
        {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                return invocationOnMock.getArguments()[0].toString();
            }
        });

        this.configurationValidator = new DefaultConfigurationValidator(i18nBean, artifactDefinitionManager);
    }

    @Test
    public void testValidateVariablesForAllTheThings()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USERNAME, "${bamboo.username.variable}");
        params.put(UiFields.USE_PASSWORD_VARIABLE, "true");
        params.put(UiFields.PASSWORD_VARIABLE, "${bamboo.password.variable}");
        params.put(UiFields.BASE_URL, "${bamboo.url.variable}");
        params.put(UiFields.USE_ATLASSIAN_ID, "false");
        params.put(UiFields.PLUGIN_ARTIFACT, "1:2:3:4:5");

        when(artifactDefinitionManager.findArtifactDefinition(2)).thenReturn(mock(ArtifactDefinition.class));

        final ErrorCollection errors = new SimpleErrorCollection();

        configurationValidator.validate(new TaskParametersMap(params), errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testValidateSimpleConfig()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USERNAME, "admin");
        params.put(UiFields.PASSWORD, "password");
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.USE_ATLASSIAN_ID, false);
        params.put(UiFields.PLUGIN_ARTIFACT, "1:2:3:4:5");

        when(artifactDefinitionManager.findArtifactDefinition(2)).thenReturn(mock(ArtifactDefinition.class));

        final ErrorCollection errors = new SimpleErrorCollection();

        configurationValidator.validate(new TaskParametersMap(params), errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testValidateAtlassianIdLogin()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.ATLASSIAN_ID_USERNAME, "admin@mydomain.com");
        params.put(UiFields.ATLASSIAN_ID_PASSWORD, "password");
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.USE_ATLASSIAN_ID, true);
        params.put(UiFields.USE_ATLASSIAN_ID_WEBSUDO, true);
        params.put(UiFields.ATLASSIAN_ID_APP_NAME, "cac");
        params.put(UiFields.PLUGIN_ARTIFACT, "1:2:3:4:5");

        when(artifactDefinitionManager.findArtifactDefinition(2)).thenReturn(mock(ArtifactDefinition.class));

        final ErrorCollection errors = new SimpleErrorCollection();

        configurationValidator.validate(new TaskParametersMap(params), errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testValidateAtlassianIdLoginWithPasswordVariableAndProductWebSudo()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.ATLASSIAN_ID_USERNAME, "admin@mydomain.com");
        params.put(UiFields.USE_ATLASSIAN_ID_PASSWORD_VARIABLE, true);
        params.put(UiFields.ATLASSIAN_ID_PASSWORD_VARIABLE, "${bamboo.thepassword}");
        params.put(UiFields.BASE_URL, "http://localhost:1990/confluence");
        params.put(UiFields.USE_ATLASSIAN_ID, true);
        params.put(UiFields.USE_ATLASSIAN_ID_WEBSUDO, false);
        params.put(UiFields.USE_PASSWORD_VARIABLE, true);
        params.put(UiFields.PASSWORD_VARIABLE, "${bamboo.confluence.password}");
        params.put(UiFields.PLUGIN_ARTIFACT, "1:2:3:4:5");

        when(artifactDefinitionManager.findArtifactDefinition(2)).thenReturn(mock(ArtifactDefinition.class));

        final ErrorCollection errors = new SimpleErrorCollection();

        configurationValidator.validate(new TaskParametersMap(params), errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testInvalidSimpleConfig()
    {
        ActionParametersMap params = new ActionParametersMapImpl(new HashMap<String, Object>());
        params.put(UiFields.USERNAME, ""); // username is required!
        params.put(UiFields.PASSWORD, ""); // password is required!
        params.put(UiFields.BASE_URL, "hello world"); // Base URL must actually be a URL!
        params.put(UiFields.USE_ATLASSIAN_ID, false);
        params.put(UiFields.PLUGIN_ARTIFACT, "1:2:3:4:5");

        when(artifactDefinitionManager.findArtifactDefinition(2)).thenReturn(null); // Artifact must exist!

        final ErrorCollection errors = new SimpleErrorCollection();

        configurationValidator.validate(new TaskParametersMap(params), errors);

        assertEquals(4, errors.getTotalErrors());
        // Base URL Error
        assertTrue(errors.getFieldErrors().containsKey(UiFields.BASE_URL));
        assertEquals("bcpd.config.form.baseUrl.error.invalid", getFieldError(errors, UiFields.BASE_URL));
        // Artifact Error
        assertTrue(errors.getFieldErrors().containsKey(UiFields.PLUGIN_ARTIFACT));
        assertEquals("bcpd.config.form.artifact.error.invalid", getFieldError(errors, UiFields.PLUGIN_ARTIFACT));
        // Username Error
        assertTrue(errors.getErrorMessages().contains("bcpd.config.form.username.error.required"));
        // Password Error
        assertTrue(errors.getErrorMessages().contains("bcpd.config.form.password.error.required"));

    }

    private String getFieldError(ErrorCollection errors, final String field)
    {
        Object o = errors.getFieldErrors().get(field);
        if (o==null)
        {
            return null;
        }
        String s = Narrow.downTo(o, String.class); //Bamboo 5.1+ has List<String>
        if (s!=null)
        {
            return s;
        }
        return ((ArrayList<String>)o).get(0);
    }
}
