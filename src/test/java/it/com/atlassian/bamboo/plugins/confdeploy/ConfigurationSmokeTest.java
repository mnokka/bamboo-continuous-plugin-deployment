package it.com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.pages.global.BambooDashboardPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskElement;
import com.atlassian.bamboo.plugins.confdeploy.config.ui.UiFields;
import com.atlassian.bamboo.plugins.confdeploy.pageobjects.DeployPluginTaskComponent;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.atlassian.pageobjects.TestedProductFactory;
import com.google.common.collect.Maps;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Exercises the configuration UI of the deploy tasks using WebDriver.
 */
public class ConfigurationSmokeTest
{
    private static final String DEPLOY_JIRA_PLUGIN = "Deploy JIRA Plugin";
    private static final String DEPLOY_CONFLUENCE_PLUGIN = "Deploy Confluence Plugin";
    private static final String DEPLOY_BAMBOO_PLUGIN = "Deploy Bamboo Plugin";
    private static final String DEPLOY_FECRU_PLUGIN = "Deploy FishEye/Crucible Plugin";

    private static final String TEST_PROJECT_KEY = "SMOKE";
    private static final String TEST_PROJECT_NAME = "Smoke Test";
    private static final String TEST_BUILD_KEY = "SAND-JOB1";
    private static final String TEST_BUILD_NAME = "Sandbox - Default Job";
    private static final String TEST_JOB_KEY  = "JOB1";
    private static final String TEST_JOB_NAME = "Default Job";

    private static BambooTestedProduct bamboo;

    @BeforeClass
    public static void init()
    {
        bamboo = TestedProductFactory.create(BambooTestedProduct.class);
    }

    @AfterClass
    public static void finish()
    {
        bamboo = null;
    }

    @Test
    public void testJiraTask() throws Exception
    {
        testAddAndDeleteTask(DEPLOY_JIRA_PLUGIN, "testJiraTask", getConfigMap());
    }

    @Test
    public void testConfluenceTask() throws Exception
    {
        testAddAndDeleteTask(DEPLOY_CONFLUENCE_PLUGIN, "testConfluenceTask", getConfigMap());
    }

    @Test
    public void testBambooTask() throws Exception
    {
        testAddAndDeleteTask(DEPLOY_BAMBOO_PLUGIN, "testBambooTask", getConfigMap());
    }

    @Test
    public void testFeCruTask() throws Exception
    {
        testAddAndDeleteTask(DEPLOY_FECRU_PLUGIN, "testFeCruTask", getConfigMap());
    }

    private void testAddAndDeleteTask(String taskName, String taskDescription, Map<String, String> taskConfig) throws Exception
    {

        TaskElement createdTask = null;
        try
        {
            // Login
            BambooDashboardPage dashboard = bamboo.gotoLoginPage().loginAsSysAdmin();
            assertTrue(dashboard.isLoggedIn());

            // Navigate to task config page
            TestBuildDetails build = new TestBuildDetails(TEST_PROJECT_NAME, TEST_PROJECT_KEY, TEST_BUILD_KEY, TEST_BUILD_NAME);
            TestJobDetails job = new TestJobDetails(build, TEST_JOB_KEY, TEST_JOB_NAME);

            JobTaskConfigurationPage configForm = bamboo.getPageBinder().navigateToAndBind(JobTaskConfigurationPage.class, job);

            configForm.addNewTask(taskName, DeployPluginTaskComponent.class, taskDescription, taskConfig);

            // Ensure the task was added successfully?
            createdTask = configForm.getTask(taskName, taskDescription); // throws exception if non-existent
            assertNotNull(createdTask);
        }
        finally
        {
            // Clean up
            if (createdTask != null)
                createdTask.delete().submit();
        }

    }

    private Map<String, String> getConfigMap()
    {
        Map<String, String> config = Maps.newHashMap();
        config.put(UiFields.BASE_URL, "http://localhost:1990/system");
        config.put(UiFields.USERNAME, "admin");
        config.put(UiFields.PASSWORD, "admin");
        // Don't need to specify the plugin artifact; a default will get selected when the form loads

        return config;
    }


}
