package testsupport;

import com.atlassian.bamboo.plugins.confdeploy.AutoDeployTask;
import com.atlassian.bamboo.plugins.confdeploy.config.TaskConfiguration;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Constructs mock instances of {@link TaskConfiguration}
 */
public class MockTaskConfigurationFactory
{
    public static final String PLUGIN_JAR = "pluginJar";
    public static final String BASE_URL = "baseUrl";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String USE_ATLASSIAN_ID = "useAtlassianId";
    public static final String USE_ATLASSIAN_ID_WEBSUDO = "useAtlassianIdWebSudo";
    public static final String ATLASSIAN_ID_USERNAME = "atlassianIdUsername";
    public static final String ATLASSIAN_ID_PASSWORD = "atlassianIdPassword";
    public static final String ATLASSIAN_ID_BASE_URL = "atlassianIdBaseUrl";
    public static final String ATLASSIAN_ID_APP_NAME = "atlassianIdAppName";
    public static final String ALLOW_CERTIFICATE_ERRORS = "allowCertificateErrors";
    public static final String ALLOW_BRANCH_BUILD_EXECUTION = "allowBranchBuildExecution";
    public static final String ENABLE_TRAFFIC_LOGGING = "enableTrafficLogging";
    public static final String PLUGIN_INSTALLATION_TIMEOUT = "pluginInstallationTimeout";

    public static TaskConfiguration getMockTaskConfiguration(@Nonnull Map<String, Object> parameters)
    {
        TaskConfiguration mockConfig = mock(TaskConfiguration.class);
        when(mockConfig.getPluginJar()).thenReturn(getParameter(PLUGIN_JAR, parameters, (File)null));
        when(mockConfig.getRemoteBaseUrl()).thenReturn(getParameter(BASE_URL, parameters, ""));
        when(mockConfig.getUsername()).thenReturn(getParameter(USERNAME, parameters, ""));
        when(mockConfig.getPassword()).thenReturn(getParameter(PASSWORD, parameters, ""));
        when(mockConfig.useAtlassianId()).thenReturn(getParameter(USE_ATLASSIAN_ID, parameters, false));
        when(mockConfig.useAtlassianIdWebSudo()).thenReturn(getParameter(USE_ATLASSIAN_ID_WEBSUDO, parameters, false));
        when(mockConfig.getAtlassianIdUsername()).thenReturn(getParameter(ATLASSIAN_ID_USERNAME, parameters, ""));
        when(mockConfig.getAtlassianIdPassword()).thenReturn(getParameter(ATLASSIAN_ID_PASSWORD, parameters, ""));
        when(mockConfig.getAtlassianIdBaseUrl()).thenReturn(getParameter(ATLASSIAN_ID_BASE_URL, parameters, ""));
        when(mockConfig.getAtlassianIdAppName()).thenReturn(getParameter(ATLASSIAN_ID_APP_NAME, parameters, ""));
        when(mockConfig.allowSSLCertificateErrors()).thenReturn(getParameter(ALLOW_CERTIFICATE_ERRORS, parameters, false));
        when(mockConfig.allowBranchBuildExecution()).thenReturn(getParameter(ALLOW_BRANCH_BUILD_EXECUTION, parameters, false));
        when(mockConfig.getEnableTrafficLogging()).thenReturn(getParameter(ENABLE_TRAFFIC_LOGGING, parameters, false));
        when(mockConfig.getPluginInstallationTimeout()).thenReturn(getParameter(PLUGIN_INSTALLATION_TIMEOUT, parameters, AutoDeployTask.DEFAULT_PLUGIN_INSTALLATION_TIMEOUT)); // TODO: Don't rely on a value being specified

        return mockConfig;
    }

    private static <T> T getParameter(final String parameterName, final Map<String, Object> parameters, final T defaultValue)
    {
        if (!parameters.containsKey(parameterName))
            return defaultValue;

        @SuppressWarnings("unchecked")
        T value = (T) parameters.get(parameterName);
        return value;
    }
}
