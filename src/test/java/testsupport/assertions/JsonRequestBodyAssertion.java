package testsupport.assertions;

import com.google.common.base.Predicate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Asserts that a {@link HttpUriRequest} body is application/json and has some expected content.
 */
public class JsonRequestBodyAssertion implements HttpUriRequestAssertion
{
    private final Predicate<JSONObject> bodyPredicate;

    public JsonRequestBodyAssertion(final Predicate<JSONObject> bodyPredicate)
    {
        this.bodyPredicate = bodyPredicate;
    }

    @Override
    public void assertRequest(HttpUriRequest request)
    {
        assertTrue(request instanceof HttpEntityEnclosingRequest);

        HttpEntityEnclosingRequest requestWithBody = (HttpEntityEnclosingRequest)request;
        HttpEntity entity = requestWithBody.getEntity();
        assertEquals("application/json; charset=utf-8", entity.getContentType().getValue());

        String s = null;
        try
        {
            s = EntityUtils.toString(entity);
        }
        catch (IOException e)
        {
            fail(e.toString());
        }

        JSONObject j = null;
        try
        {
            j = new JSONObject(s);
        }
        catch (JSONException e)
        {
            fail(e.toString());
        }

        bodyPredicate.apply(j);
    }
}
