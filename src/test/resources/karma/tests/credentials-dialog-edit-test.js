/**
 * Test module for updating the task configuration using the edit credentials dialog.
 */
define(['edit-task'], function(confDeploy) {

    module("Test Credentials Dialog - update task configuration", {
        setup: function() {
            // Render the HTML fixture
            AJS.$("body").append(window.__html__['src/test/resources/karma/fixtures/blank-form.html']);
        },
        teardown: function() {
            AJS.$("body").empty();
        }
    });

    test("Product login", function() {
        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Enter values
        AJS.$("#bcpd-login-useAtlassianId-productLogin").click();
        AJS.$("#bcpd-login-username").val("sysadmin");
        AJS.$("#bcpd-login-password").val("thepassword");

        // Save & close the dialog
        AJS.$(".button-panel-submit-button", "div.aui-dialog").click();

        // Dialog should now be gone.
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0);

        // Parent form should have been updated.
        equal(AJS.$("#bcpd_config_username").val(), "sysadmin");
        equal(AJS.$("#bcpd_config_usePasswordVariable").val(), "false");
        equal(AJS.$("#bcpd_config_password").val(), "thepassword");
        equal(AJS.$("#bcpd_config_passwordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianId").val(), "false");
        equal(AJS.$("#bcpd_config_atlassianIdBaseURL").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdUsername").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdPassword").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdPasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdWebSudo").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdAppName").val(), "");
    });

    test("Product login with password variable", function() {
        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Enter values
        AJS.$("#bcpd-login-useAtlassianId-productLogin").click();
        AJS.$("#bcpd-login-username").val("sysadmin");
        AJS.$("#bcpd-login-usePasswordVariable").click();
        AJS.$("#bcpd-login-passwordVariable").val("${bamboo.some.password}");

        // Save & close the dialog
        AJS.$(".button-panel-submit-button", "div.aui-dialog").click();

        // Dialog should now be gone.
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0);

        // Parent form should have been updated.
        equal(AJS.$("#bcpd_config_username").val(), "sysadmin");
        equal(AJS.$("#bcpd_config_usePasswordVariable").val(), "true");
        equal(AJS.$("#bcpd_config_password").val(), "");
        equal(AJS.$("#bcpd_config_passwordVariable").val(), "${bamboo.some.password}");
        equal(AJS.$("#bcpd_config_useAtlassianId").val(), "false");
        equal(AJS.$("#bcpd_config_atlassianIdBaseURL").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdUsername").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdPassword").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdPasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdWebSudo").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdAppName").val(), "");
    });

    test("Atlassian ID Login with Atlassian ID WebSudo", function() {
        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Enter values
        AJS.$("#bcpd-login-useAtlassianId-atlassianId").click();
        AJS.$("#bcpd-login-username").val("sysadmin@atlassian.com");
        AJS.$("#bcpd-login-password").val("my_secret_password");
        AJS.$("#bcpd-websudo-method-lasso").click();
        AJS.$("#bcpd-websudo-appname").val("app");

        // Save & close the dialog
        AJS.$(".button-panel-submit-button", "div.aui-dialog").click();

        // Dialog should now be gone.
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0);

        // Parent form should have been updated.
        equal(AJS.$("#bcpd_config_username").val(), "");
        equal(AJS.$("#bcpd_config_usePasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_password").val(), "");
        equal(AJS.$("#bcpd_config_passwordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianId").val(), "true");
        equal(AJS.$("#bcpd_config_atlassianIdBaseURL").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdUsername").val(), "sysadmin@atlassian.com");
        equal(AJS.$("#bcpd_config_atlassianIdPassword").val(), "my_secret_password");
        equal(AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val(), "false");
        equal(AJS.$("#bcpd_config_atlassianIdPasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdWebSudo").val(), "true");
        equal(AJS.$("#bcpd_config_atlassianIdAppName").val(), "app");
    });

    test("Atlassian ID Login with Product WebSudo", function() {

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Enter values
        AJS.$("#bcpd-login-useAtlassianId-atlassianId").click();
        AJS.$("#bcpd-login-username").val("sysadmin@atlassian.com");
        AJS.$("#bcpd-login-password").val("my_secret_password");
        AJS.$("#bcpd-websudo-method-product").click();
        AJS.$("#bcpd-websudo-password").val("websudo-password");

        // Save & close the dialog
        AJS.$(".button-panel-submit-button", "div.aui-dialog").click();

        // Dialog should now be gone.
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0);

        // Parent form should have been updated.
        equal(AJS.$("#bcpd_config_username").val(), "");
        equal(AJS.$("#bcpd_config_usePasswordVariable").val(), "false");
        equal(AJS.$("#bcpd_config_password").val(), "websudo-password");
        equal(AJS.$("#bcpd_config_passwordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianId").val(), "true");
        equal(AJS.$("#bcpd_config_atlassianIdBaseURL").val(), "");
        equal(AJS.$("#bcpd_config_atlassianIdUsername").val(), "sysadmin@atlassian.com");
        equal(AJS.$("#bcpd_config_atlassianIdPassword").val(), "my_secret_password");
        equal(AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val(), "false");
        equal(AJS.$("#bcpd_config_atlassianIdPasswordVariable").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdWebSudo").val(), "false");
        equal(AJS.$("#bcpd_config_atlassianIdAppName").val(), "");
    });

    test("Atlassian ID Login with Product WebSudo using password variables", function() {

        // Load the dialog
        AJS.$("#bcpd_config_credentials").click();

        // Enter values
        AJS.$("#bcpd-login-useAtlassianId-atlassianId").click();
        AJS.$("#bcpd-login-username").val("sysadmin@atlassian.com");
        AJS.$("#bcpd-login-usePasswordVariable").click();
        AJS.$("#bcpd-login-passwordVariable").val("${bamboo.password1}");
        AJS.$("#bcpd-login-atlassianIdBaseUrl").val("https://id.stg.iam.atlassian.com");
        AJS.$("#bcpd-websudo-method-product").click();
        AJS.$("#bcpd-websudo-usePasswordVariable").click();
        AJS.$("#bcpd-websudo-passwordVariable").val("${bamboo.password2}");

        // Save & close the dialog
        AJS.$(".button-panel-submit-button", "div.aui-dialog").click();

        // Dialog should now be gone.
        var dialog = AJS.$("h2.bcpd-dialog-title");
        equal(dialog.length, 0);

        // Parent form should have been updated.
        equal(AJS.$("#bcpd_config_username").val(), "");
        equal(AJS.$("#bcpd_config_usePasswordVariable").val(), "true");
        equal(AJS.$("#bcpd_config_password").val(), "");
        equal(AJS.$("#bcpd_config_passwordVariable").val(), "${bamboo.password2}");
        equal(AJS.$("#bcpd_config_useAtlassianId").val(), "true");
        equal(AJS.$("#bcpd_config_atlassianIdBaseURL").val(), "https://id.stg.iam.atlassian.com");
        equal(AJS.$("#bcpd_config_atlassianIdUsername").val(), "sysadmin@atlassian.com");
        equal(AJS.$("#bcpd_config_atlassianIdPassword").val(), "");
        equal(AJS.$("#bcpd_config_useAtlassianIdPasswordVariable").val(), "true");
        equal(AJS.$("#bcpd_config_atlassianIdPasswordVariable").val(), "${bamboo.password1}");
        equal(AJS.$("#bcpd_config_useAtlassianIdWebSudo").val(), "false");
        equal(AJS.$("#bcpd_config_atlassianIdAppName").val(), "");
    })
});
